#pragma once
class Body;
class Material
{
public:
	Material();
	void setOwner( Body* pBody ){ mOwner = pBody; }
	void setDensity( const float& pDensity ){ mDensity = pDensity; }
	float getDensity(){ return mDensity; }
	void setStaticFriction( const float& pSFriction ){ mStaticFriction = pSFriction; }
	float getStaticFriction(){ return mStaticFriction; }
	void setKineticFriction( const float& pKFriction ){ mKineticFriction = pKFriction; }
	float getKineticFriction(){ return mKineticFriction; }
	void setRestitution( const float& pRestitution ){ mRestitution = pRestitution; }
	float getRestitution(){ return mRestitution; }
	~Material();
private:
	float mDensity; //mass is calculated via density and volume (see dimensions in Shape objects) 1.0 for completely solid, 0.0 for completely gaseous
	float mStaticFriction; //dictates how much "grip" the material has when not moving
	float mKineticFriction; //dictates how much "slide" the object has when moving. This is a loss of energy
	float mRestitution; //Dictates how "bouncy" an object is ie how much energy is lost during collisions. 1.0 for perfect and 0.0 for imperfect
	Body* mOwner;
};

