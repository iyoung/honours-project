#include "Timer.h"
#include "Application.h"
#include "TimerEvent.h"


Timer::Timer( int timeStepPerSecond )
{
	mTimeStep = 1.0f/ timeStepPerSecond;
	mTimeStepCycles = (clock_t)(CLOCKS_PER_SEC *mTimeStep);
	mAccumulator = 0;
	mFrameAccumulator = 0;
}

void Timer::update( Application & pApp )
{
	clock_t timeStep = clock();
	clock_t dt = (timeStep - mAccumulator);
	if ( mFrameAccumulator > mTimeStepCycles )
	{
		float delta = (float)(mFrameAccumulator / CLOCKS_PER_SEC);
		TimerEvent t( mTimeStep );
		pApp.handleEvent( t );
		mFrameAccumulator = 0;
	}
	else
	{
		mFrameAccumulator += dt;
	}
	mAccumulator += dt;
	
}

float Timer::getCurrentTimeStamp()
{
	clock_t ts = clock();
	mLastRequestedTimeStamp = ts;
	return (float)mLastRequestedTimeStamp/CLOCKS_PER_SEC;
}


float Timer::getTimeSinceLastRequest()
{
	clock_t dt = clock();
	dt = dt - mLastRequestedTimeStamp;
	mLastRequestedTimeStamp = dt;
	return (float)dt/CLOCKS_PER_SEC;
}

void Timer::initTimer()
{
	mAccumulator = clock();
}

Timer::~Timer()
{
}
