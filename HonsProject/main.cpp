#include <SDL.h>
#include "Application.h"
#include "CollisionMode.h"
int main(int argc, char* args[])
{
	Application* app = new Application();
	if(app->init(CPU_MULTI_THREAD) == NO_ERROR)
		app->run();
	delete app;
	return 0;
}