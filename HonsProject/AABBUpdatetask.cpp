#include "AABBUpdatetask.h"
#include "SAPMultiCore.h"
#include "Shape.h"
#include "Body.h"
#include <stdio.h>
AABBUpdatetask::AABBUpdatetask(SAP_AABB* pBox)
{
	box = pBox;
	status = true;
}

void AABBUpdatetask::setData(void * pData)
{
	box = (SAP_AABB*)pData;
}

void AABBUpdatetask::execute()
{
	Body* body = box->m_Body;
	Shape* shape = body->getShape();
	Entity* entity = body->getOwner();

	float axes[] = { 1.0f,1.0f,1.0f };
	glm::vec3 p = entity->getPosition();
	glm::quat q = entity->getOrientation();
	//if shape is an OBB
	if (shape->getShapeType() == SHAPE_CUBOID)
	{
		Cuboid* c = (Cuboid*)shape;
		std::vector<glm::vec3> verts = c->getVerts();
		glm::vec3 points[8]; 

		for (size_t i = 0; i < verts.size(); i++)
		{
			points[i] = p + glm::vec3((q*glm::vec4(verts[i],1.0f)));
		}

		for (int axis = 0; axis < 3; axis++)
		{
			glm::vec3 pos = points[0];
			float min = pos[axis];
			float max = pos[axis];
			for (int i = 1; i < 8; i++)
			{
				if (points[i][axis] < min)
				{
					min = points[i][axis];
				}
				else if (points[i][axis] > max)
				{
					max = points[i][axis];
				}
			}
			box->m_MinExtents[axis] = min;
			box->m_MaxExtents[axis] = max;
		}
	}
	//else is a sphere
	else if (shape->getShapeType() == SHAPE_SPHERE)
	{
		Sphere* s = (Sphere*)shape;
		for (int axis = 0; axis < 3; axis++)
		{
			box->m_MinExtents[axis] = p[axis] - s->getRadius();
			box->m_MaxExtents[axis] = p[axis] + s->getRadius();
		}
	}
	//else must be a plane
	else
	{
		Plane* c = (Plane*)shape;
		glm::vec3 points[4];
		std::vector<glm::vec3> verts = c->getVerts();
		for (size_t i = 0; i < verts.size(); i++)
		{
			points[i] = p + glm::vec3((q*glm::vec4(verts[i], 1.0f)));
		}
		for (int axis = 0; axis < 3; axis++)
		{
			glm::vec3 pos = points[0];
			float min = pos[axis];
			float max = pos[axis];
			for (int i = 1; i < 4; i++)
			{
				if (points[i][axis] < min)
				{
					min = points[i][axis];
				}
				else if (points[i][axis] > max)
				{
					max = points[i][axis];
				}
			}
			box->m_MinExtents[axis] = min;
			box->m_MaxExtents[axis] = max;
		}
	}
	status = true;
}

void * AABBUpdatetask::getResults()
{
	return nullptr;
}

bool AABBUpdatetask::getCompleteStatus()
{
	return false;
}


AABBUpdatetask::~AABBUpdatetask()
{
}
