#pragma once
#include <vector>
#include <tuple>
#include <string>
#include <functional>
#include <fstream>
#include <chrono>
#include <ctime>

#define PROFILER Profiler::getInstance()

struct TimerSection
{
	std::string mSectionName;
	double mWorst;
	double mBest;
	double mAverage;
	double mTotal;
	int mProfileCount;
	std::vector<TimerSection> mSubSections;
	TimerSection()
	{
		mWorst = 0.0;
		mBest = 0.0;
		mAverage = 0.0;
		mTotal = 0.0;
		mProfileCount = 0;
	}
	TimerSection( const std::string& pSectionName )
	{
		mWorst = 0.0;
		mBest = DBL_MAX;
		mAverage = 0.0;
		mProfileCount = 0;
		mTotal = 0.0;
		mSectionName = pSectionName;
	}
	~TimerSection()
	{
		mSubSections.clear();
	}

	friend std::ostream& operator << ( std::ostream& os, const TimerSection& section )
	{
		os << section.mSectionName << ": " << std::endl;
		os << "    Best: " << section.mBest << std::endl;
		os << "    Worst: " << section.mWorst << std::endl;
		os << "    Average: " << (section.mTotal/section.mProfileCount) << std::endl;
		os << "    Total: " << section.mTotal<< std::endl << std::endl;
		if ( !section.mSubSections.empty() )
		{
			for ( size_t i = 0; i < section.mSubSections.size(); i++ )
			{
				os << section.mSubSections[ i ];
			}
		}
		return os;
	}
};

struct SectionName :public std::binary_function<TimerSection , std::string , bool>
{
	bool operator()( const TimerSection& section , const std::string& sectionName )const
	{
		return section.mSectionName == sectionName;
	}
};

class Profiler
{
public:
	static Profiler* getInstance();
	void startApplicationProfile( const std::string& pAppName );
	void startProfileSection( const std::string& pSectionName );
	bool endProfileSection( const std::string& pSectionName );
	void startProfileSubSection( const std::string& pSectionName , const std::string& pSubSectionName );
	bool endProfileSubSection( const std::string& pSectionName , const std::string& pSubSectionName );
	void endApplicationProfile( const std::string& pFileOutput );
	~Profiler();
private:
	Profiler();
	static Profiler* mInstance;
	TimerSection mAppTimes;
	std::chrono::time_point<std::chrono::system_clock> mAppStart;
	std::chrono::time_point<std::chrono::system_clock> mAppEnd;
	std::chrono::time_point<std::chrono::system_clock> mSectionStart;
	std::chrono::time_point<std::chrono::system_clock> mSectionEnd;
	std::chrono::time_point<std::chrono::system_clock> mSubSectionStart;
	std::chrono::time_point<std::chrono::system_clock> mSubSectionEnd;
};

