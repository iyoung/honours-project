#pragma once
#include "Event.h"
#include "GLRenderer.h"
#include "Controller.h"
#include <SDL.h>
#include "CollisionMode.h"
#include <SDL_thread.h>
//forward declarations
class CollisionSolver;
class PhysicsSystem;
class Timer;
class EventHandler;
class EntityFactory;
class Entity;
//basic error handling stuff
enum ApplicationError
{
	NO_ERROR = 0,
	GL_ERROR = 1,
	SDL_ERROR = 2,
	CL_CUDA_ERROR = 3,
	FILE_READ_ERROR = 4,
	FILE_WRITE_ERROR = 5,

};

//Main Application declaration: This is the high level application, that manages all the other modules.
class Application
{
public:
	Application();
	ApplicationError init(CollisionMode pMode);
	ApplicationError run();
	void handleEvent(Event& pEvent);
	void cleanup();
	~Application();
private:
	void OutputResultsToFile();
	void construcScene();
	bool mRunning; //startup/shutdown mechanism
	CollisionSolver* mCollisionSolver; //handles scene management, collision detection
	GLRenderer* mRenderer; //handles rendering of scene
	Controller* mController; //parses sdl key events
	PhysicsSystem* mPhysicsSolver; //handles all physics related processing
	Timer* mTimer; //keeps time for the application
	EventHandler* mEventHandler; //handles generic events
	EntityFactory* mFactory;
	float mBestTime , mWorstTime , mAverageTime; //for performance profiling
	bool mTimeRecorded; //this is false when the app starts
	long long mNumCycles; //records the number of cycles in the simulation
	std::vector<Entity*> mEntities;
	Entity* mSphere;
	SDL_mutex* mLock;
	friend class SATTask;
	std::string mAppName;
};

