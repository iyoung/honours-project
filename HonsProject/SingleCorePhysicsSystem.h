#pragma once
#include "PhysicsSystem.h"
#include <vector>
class Body;
class SingleCorePhysicsSystem :
	public PhysicsSystem
{
public:
	SingleCorePhysicsSystem();
	virtual void update( const float& p_DeltaTime , Application& pApp );
	virtual void initWorld( const glm::vec3& pGravity, float pTimeStep);
	virtual void setPairManager( std::shared_ptr<PairManager> pManager );
	virtual void addEntity( Entity* pEntity );
	virtual void handleEvent( Event& pEvent );
	virtual void shutDown();
	virtual ~SingleCorePhysicsSystem();
private:
	void resolveImpulses( float pDeltaTimeS );
	void resolveLinearImpulses( Body* pBody, float pDeltaTimeS );
	void resolveAngularImpulses( Body* pBody, float pDeltaTimeS );
	std::shared_ptr<PairManager> mPairManager;
	std::vector<Body*> mBodies;
	glm::vec3 mGravity;
	float mTimeStep;
};

