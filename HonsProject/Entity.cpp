#include "Entity.h"
#include <glm\gtx\quaternion.hpp>
#include <glm\gtc\matrix_transform.hpp>
#include "Shape.h"
Entity::Entity()
{
	mMoved = true;
}
void Entity::setPosition(glm::vec3 pPosition)
{
	mPosition = pPosition; 
	mMoved = true;
}
void Entity::update( const float& pDeltaTimeS )
{
	if ( mBody->isAwake() )
	{
		glm::vec3 v = mBody->getVelocity();
		glm::vec3 W = mBody->getAngularVelocity();
		//if the kinetic energy of the entity is below a threshold, put it to sleep
		float Ek = glm::dot(v, v) + glm::dot(W, W);
		if (Ek < 0.1f)
		{
			mBody->sleep();
		}
		else
		{
			mPosition += v * pDeltaTimeS;
			mMoved = true;

			//glm::vec3 axis = glm::normalize(W);
			//float angle = glm::length(W);
			glm::quat w = glm::quat(W);
			glm::quat ident;
			glm::quat rotation = glm::mix(ident, w, pDeltaTimeS);
			mOrientation = rotation * mOrientation;
		}

	}
	if ( mMoved )
		setTransform();
}

void Entity::setTransform()
{
	mTransform = glm::mat4( 1.0f );
	glm::mat4 translate = glm::translate( mTransform , mPosition );
	glm::mat4 rotation = glm::toMat4( mOrientation );

	glm::mat4 scale(1.0);
	if ( mBody->getShape()->getShapeType() == SHAPE_PLANE )
	{
		Plane* shape = dynamic_cast< Plane* >( mBody->getShape() );
		scale = glm::scale(scale, glm::vec3( shape->getWidth() , 1.0f , shape->getLength() ) );
	}
	else
	{
		scale = glm::scale(scale, glm::vec3( 1.0f ));
	}
	mTransform = translate*rotation*scale;
	mMoved = false;
}
glm::mat4& Entity::getTransform()
{
	return mTransform;
}
void Entity::cleanup()
{
	delete mBody;
	delete mAppearance;
}
Entity::~Entity()
{
	cleanup();
}
