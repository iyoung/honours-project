#pragma once
#include "PairManager.h"
#include <memory>
#include <vector>
#include "CollisionMode.h"
#include "CollisionTypes.h"
#include "SATMultiCore.h"
#include "SAPMultiCore.h"
class Entity;
class BroadPhase;
class NarrowPhase;
class Application;



class CollisionSolver
{
public:
	CollisionSolver();
	void addEntity( Entity* pEntity );
	void init( CollisionMode pMode );
	void update( Application& pApp );
	void setSolverMode( CollisionMode pMode );
	void removeEntity( Entity* pEntity );
	void setBatchMode();
	void EndBatchMode();
	void shutDown();
	~CollisionSolver();

private:
	std::vector<std::unique_ptr<BroadPhase>> mBroadPhase;
	std::vector<std::unique_ptr<NarrowPhase>> mNarrowPhase;
	std::shared_ptr<PairManager> mPairmanager;
	std::vector<Entity*> mEntities;
	CollisionMode mMode;
	std::string mModeName;
};

