#include "SAPSingleCore.h"
#include <glm\glm.hpp>
#include "Body.h"
#include "Entity.h"
#include "Shape.h"
#include <algorithm>
#include "PairManager.h"
#include <stdlib.h>
#include "Profiler.h"


void SAPSingleCore::updateAABB(SAP_AABB& p_AABB)
{
	Body* body = p_AABB.m_Body;
	Shape* shape = body->getShape();
	Entity* entity = body->getOwner();

	float axes[] = { 1.0f,1.0f,1.0f };
	glm::vec3 p = entity->getPosition();
	glm::mat3 t = glm::mat3(glm::toMat4(entity->getOrientation()));
	//if shape is an OBB
	if (shape->getShapeType() == SHAPE_CUBOID)
	{
		Cuboid* c = (Cuboid*)shape;
		glm::vec3 points[8] = {
			p + t*glm::vec3(-c->getWidth()*0.5f,-c->getHeight()*0.5f,-c->getLength()*0.5f), //bottom left front
			p + t*glm::vec3(-c->getWidth()*0.5f,-c->getHeight()*0.5f,c->getLength()*0.5f),  //bottom left back
			p + t*glm::vec3(c->getWidth()*0.5f,-c->getHeight()*0.5f,-c->getLength()*0.5f),  //bottom right front
			p + t*glm::vec3(c->getWidth()*0.5f,-c->getHeight()*0.5f,c->getLength()*0.5f),  //bottom right back
			p + t*glm::vec3(-c->getWidth()*0.5f,c->getHeight()*0.5f,-c->getLength()*0.5f), //top left front
			p + t*glm::vec3(-c->getWidth()*0.5f,c->getHeight()*0.5f,c->getLength()*0.5f),  //top left back
			p + t*glm::vec3(c->getWidth()*0.5f,c->getHeight()*0.5f,-c->getLength()*0.5f),  //top right front
			p + t*glm::vec3(c->getWidth()*0.5f,c->getHeight()*0.5f,c->getLength()*0.5f) }; //top left back

		for (int axis = 0; axis < 3; axis++)
		{
			glm::vec3 pos =points[0];
			float min = pos[axis];
			float max = pos[axis];
			for (int i = 1; i < 8; i++)
			{
				if (points[i][axis] < min)
				{
					min = points[i][axis];
				}
				else if (points[i][axis] > max)
				{
					max = points[i][axis];
				}
			}
			p_AABB.m_MinExtents[axis] = min;
			p_AABB.m_MaxExtents[axis] = max;
		}
	}
	//else is a sphere
	else if (shape->getShapeType() == SHAPE_SPHERE)
	{
		Sphere* s = (Sphere*)shape;
		for (int axis = 0; axis < 3; axis++)
		{
			p_AABB.m_MinExtents[axis] = p[axis] - s->getRadius();
			p_AABB.m_MaxExtents[axis] = p[axis] + s->getRadius();
		}
	}
	//else must be a plane
	else
	{
		Plane* c = (Plane*)shape;
		glm::vec3 points[4] = {
			p + t*glm::vec3(-c->getWidth()*0.5f,0.0f,-c->getLength()*0.5f), //left front
			p + t*glm::vec3(-c->getWidth()*0.5f,0.0f,c->getLength()*0.5f),  //left back
			p + t*glm::vec3(c->getWidth()*0.5f,0.0f,-c->getLength()*0.5f),   //right front
			p + t*glm::vec3(c->getWidth()*0.5f,0.0f,c->getLength()*0.5f),  //right back
		};
		for (int axis = 0; axis < 3; axis++)
		{
			glm::vec3 pos = points[0];
			float min = pos[axis];
			float max = pos[axis];
			for (int i = 1; i < 4; i++)
			{
				if (points[i][axis] < min)
				{
					min = points[i][axis];
				}
				else if (points[i][axis] > max)
				{
					max = points[i][axis];
				}
			}
			p_AABB.m_MinExtents[axis] = min;
			p_AABB.m_MaxExtents[axis] = max;
		}
	}
}

SAPSingleCore::SAPSingleCore()
{
}

void SAPSingleCore::update( Application & pApp )
{
	PROFILER->startProfileSubSection( "Single_Thread_BroadPhase" , "Update_AABB_SAP" );
	std::for_each(m_AABBs.begin(), m_AABBs.end(), updateAABB);
	qsort(m_AABBs.data(), m_AABBs.size(), sizeof(SAP_AABB), SAPSingleCore::comp_AABB);
	PROFILER->endProfileSubSection( "Single_Thread_BroadPhase" , "Update_AABB_SAP" );
	PROFILER->startProfileSubSection( "Single_Thread_BroadPhase" , "Pair_Generation" );
	if (m_AABBs.size() > 1)
	{
		for (size_t i = 0; i < m_AABBs.size(); i++)
		{
			//for each AABB in the sorted array, check it against all AABB's following it's position
			for (size_t j = i + 1; j < m_AABBs.size(); j++)
			{
				if (m_AABBs[i].m_MaxExtents[0] < m_AABBs[j].m_MinExtents[0])
					break;
				else
				{
					if (AABBTest(m_AABBs[i], m_AABBs[j]))
					{
						mPairManager->addpair(Pair(m_AABBs[i].m_Body, m_AABBs[j].m_Body));
					}
				}
			}
		}
	}
	PROFILER->endProfileSubSection( "Single_Thread_BroadPhase" , "Pair_Generation" );
}

void SAPSingleCore::addEntity( Entity * pEntity )
{
	//generate 6 extents (2 for each axis: 3 min, 3 max).
	//insert extents into extent vectors
	//sort all vectors into ascending order
	Body* b = pEntity->getBody();
	mBodies.push_back(b);
	m_AABBs.push_back(SAP_AABB());
	m_AABBs.back().m_Body = b;
	m_AABBs.back().m_ID = b->getID();
	
	if (!mBatchInsert && m_AABBs.size() > 1)
	{
		updateAABB(m_AABBs.back());
		qsort(m_AABBs.data(), m_AABBs.size(), sizeof(SAP_AABB), SAPSingleCore::comp_AABB);
	}
}

void SAPSingleCore::removeEntity( Entity* pEntity )
{
	//go through each axis vector
	//and remove
	// the evaluation function

	//and update the lists

}

void SAPSingleCore::setPairManager( std::shared_ptr<PairManager> pManager )
{
	mPairManager = pManager;
}

void SAPSingleCore::cleanUp()
{
	mBodies.clear();
	m_AABBs.clear();
}

bool SAPSingleCore::AABBTest(SAP_AABB & A, SAP_AABB & B)
{
	for (int axis = 0; axis < 3; axis++)
	{
		if (A.m_MinExtents[axis] > B.m_MaxExtents[axis] || A.m_MaxExtents[axis] < B.m_MinExtents[axis])
			return false;
	}
	return true;
}

void SAPSingleCore::endBatchInsertMode()
{
	std::for_each(m_AABBs.begin(), m_AABBs.end(), updateAABB);
	qsort(m_AABBs.data(), m_AABBs.size(), sizeof(SAP_AABB), SAPSingleCore::comp_AABB);
}

SAPSingleCore::~SAPSingleCore()
{
}

int SAPSingleCore::comp_AABB(const void * a, const void * b)
{
	{
		SAP_AABB A = *(SAP_AABB*)a;
		SAP_AABB B = *(SAP_AABB*)b;
		float minA = A.m_MinExtents[0];
		float minB = B.m_MinExtents[0];
		if (minA < minB) return -1;
		if (minA > minB) return 1;
		else return 0;
	}
}
