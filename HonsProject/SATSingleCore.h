#pragma once
#include "NarrowPhase.h"
#include "PairManager.h"
#include <glm\glm.hpp>
#include "CollisionTypes.h"

class SATSingleCore :
	public NarrowPhase
{
public:
	SATSingleCore();
	virtual void update( Application& pApp );
	virtual void setPairManager( std::shared_ptr<PairManager> pManager );
	virtual void shutDown();
	virtual ~SATSingleCore();
protected:
	void sphereVSplane(Pair& pPair, Application& pApp);
	void sphereVScuboid(Pair& pPair, Application& pApp);
	void cuboidVSplane(Pair& pPair, Application& pApp);
	void cuboidVScuboid(Pair& pPair, Application& pApp);
	void computeNearestEdge(glm::vec3* data, int size, glm::vec3* line, glm::vec3 circleCentre);
	glm::vec3 SphereVSplaneContact(glm::vec3 pNormal, glm::vec3 sphereCentre, float radius, float planeDepth);
	int computeNearestPointToSphere(glm::vec3 data[], int size, glm::vec3 pCircleCentre, float pRadius);
	int computeFarthestPoint( glm::vec3 points[], int numPoints, glm::vec3 vector );
	glm::vec3 computeContactPointSphereLine( glm::vec3 p_PointA , glm::vec3 p_PointB , glm::vec3 p_CircleCentre , float p_CircleRadius );
	float PPDistanceSquared(glm::vec3& pStart, glm::vec3& pEnd);
	bool sphereVsLine(glm::vec3 p_PointA, glm::vec3 p_PointB, glm::vec3 p_CircleCentre, float p_CircleRadius, glm::vec3& pNormal);
	glm::vec3 nearestPointCircleLine( glm::vec3 pStart , glm::vec3 pEnd , glm::vec3 pCircleCentre );
	int computeMinProjection(glm::vec3 data[], int size, glm::vec3 axis);
	int computeMaxProjection(glm::vec3 data[], int size, glm::vec3 axis);
	float computeMinProj(glm::vec3 data[], int size, glm::vec3 axis);
	float computeMaxProj(glm::vec3 data[], int size, glm::vec3 axis);
	bool planeSphereIntersects(glm::vec3& normal, float& planePosition, float& radius, glm::vec3& sphereCentre, float& penetration, glm::vec3& contact);
	bool lineSphereIntersects(glm::vec3& line, glm::vec2& extents, float& radius, glm::vec3& sphereCentre, float penetration, glm::vec3& contact);
	bool pointSphereIntersects(glm::vec3& point, float& radius, glm::vec3& sphereCentre, float& penetration);
	std::shared_ptr<PairManager> mPairManager;
};

