#pragma once
#include "BroadPhase.h"
#include <vector>
#include "Body.h"
#include "Entity.h"
#include <iostream>
#include "CollisionTypes.h"
#include "SAPSingleCore.h"

class SAPMultiCore :
	public BroadPhase
{
public:
	SAPMultiCore();
	static const int sortAxis = 0;
	virtual void update( Application& pApp ) ;
	virtual void addEntity( Entity* pEntity ) ;
	virtual void removeEntity( Entity* pEntity );
	virtual void setPairManager( std::shared_ptr<PairManager> pManager );
	virtual void init() { ; }
	virtual void cleanUp();
	virtual void setBatchInsertMode() { mBatchInsert = true; }
	virtual void endBatchInsertMode();
	virtual ~SAPMultiCore();
protected:
	friend class SAPTask;
	static bool AABBTest(SAP_AABB& A, SAP_AABB& B);
	std::vector<SAP_AABB> m_AABBs;
	std::shared_ptr<PairManager> mPairManager;
	std::vector<Body*> mBodies;
	bool mBatchInsert;
};

