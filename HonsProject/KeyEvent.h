#pragma once
#include "Event.h"
#include <SDL.h>
enum KeyState
{
	KEY_PRESSED,
	KEY_HELD,
	KEY_RELEASED,
	MOUSE_MOVE,
	MOUSE_BUTTON_PRESSED,
	MOUSE_BUTTON_RELEASED,
};
class KeyEvent :
	public Event
{
public:
	KeyEvent( KeyState pState, int pMMotionX , int pMMotionY , Uint8 pMouseButtonIndex );
	KeyEvent( SDL_Scancode pKey, KeyState pState);
	virtual EventType getEventType() { return EVENT_KEYPRESS; }
	SDL_Scancode getKey() { return mKey; }
	KeyState getState() { return mState; }
	int getMouseMotionX() { return mMouseMotionX; }
	int getMouseMotionY() { return mMouseMotionY; }
	bool isMouseEvent() { return mMouse; }
	Uint8 getMouseButtonIndex() { return mMouseButtonIndex; }
	virtual ~KeyEvent();
private:
	SDL_Scancode mKey;
	KeyState mState;
	int mMouseMotionX;
	int mMouseMotionY;
	Uint8 mMouseButtonIndex;
	bool mMouse;
};

