#pragma once
#include <ctime>
class Application;
class Timer
{
public:
	Timer(int timeStepPerSecond);
	void update( Application& pApp );
	float getCurrentTimeStamp();
	float getTimeSinceLastRequest();
	void initTimer();
	~Timer();
private:
	clock_t mAccumulator;
	float mTimeStep;
	clock_t mTimeStepCycles;
	clock_t mLastRequestedTimeStamp;
	clock_t mFrameAccumulator;
	clock_t m_timeStepAccumulator;
};

