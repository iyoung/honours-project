#pragma once
#include <glm\glm.hpp>
struct LightData
{
    glm::vec4 mDiffuse;
    glm::vec4 mPosition;
	glm::vec3 mAttenuation;
};

enum LightType
{
    DIRECTIONAL,
    POINT_LIGHT,
    SPOT_LIGHT
};

class Light
{
public:
    Light(void);
    void setDiffuse(const glm::vec4& pDiffuse);
    void setPosition(const glm::vec4& pPosition);
	void setAttenuation(const glm::vec3& pAttenuation);
	void move(const glm::vec3& pOffset);
    LightData& getData();
    virtual ~Light(void);
private:
    LightData mData;
};

