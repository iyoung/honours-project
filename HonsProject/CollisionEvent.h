#pragma once
#include "Event.h"
#include "Body.h"
#include <memory>

struct CollisionData
{
	glm::vec3 mNormal;
	float mPenetrationDepth;
	Body* A;
	Body* B;
	glm::vec3 mContactPointA;
	glm::vec3 mContactPointB;
	float mDeltaTime;
};

class CollisionEvent :
	public Event
{
public:
	CollisionEvent() { ; }
	CollisionEvent( EventType pCollisionType, CollisionData& pData);
	virtual EventType getEventType();
	void setCollisionData(EventType pCollisionType, CollisionData& pData);
	CollisionData& getData() { return mData; }
	virtual ~CollisionEvent();
private:
	EventType mEventType;
	CollisionData mData;
};

