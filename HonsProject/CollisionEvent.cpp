#include "CollisionEvent.h"



CollisionEvent::CollisionEvent( EventType pCollisionType , CollisionData& pData )
{
	mData = pData;
	mEventType = pCollisionType;
}

EventType CollisionEvent::getEventType()
{
	return mEventType;
}

void CollisionEvent::setCollisionData(EventType pCollisionType, CollisionData & pData)
{
	mEventType = pCollisionType;
	mData = pData;
}


CollisionEvent::~CollisionEvent()
{
}
