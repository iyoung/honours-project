#pragma once
enum EventType
{
	EVENT_NO_EVENT = 0,
	EVENT_TIMESTEP ,
	EVENT_NO_COLLISION,
	EVENT_COLLISION_BEGIN,
	EVENT_COLLISION_END,
	EVENT_COLLISION,
	EVENT_KEYPRESS,
	EVENT_SHUTDOWN,
	EVENT_PROJECTILE,
};

class Event
{
public:
	Event();
	virtual EventType getEventType() { return EVENT_SHUTDOWN; }
	virtual ~Event();
};

