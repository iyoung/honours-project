#pragma once
#include <memory>
class Application;
class PairManager;
class Entity;

class NarrowPhase
{
public:
	NarrowPhase();
	virtual void update( Application& pApp ) = 0;
	virtual void setPairManager( std::shared_ptr<PairManager> pManager ) = 0;
	virtual void shutDown() = 0;
	virtual ~NarrowPhase();
};

