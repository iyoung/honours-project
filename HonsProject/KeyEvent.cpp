#include "KeyEvent.h"



KeyEvent::KeyEvent( KeyState pState, int pMMotionX = 0 , int pMMotionY = 0 , Uint8 pMouseButtonIndex = 0 )
{
	mState = pState;
	mMouseMotionX = pMMotionX;
	mMouseMotionY = pMMotionY;
	mMouseButtonIndex = pMouseButtonIndex;
	mMouse = true;
}


KeyEvent::KeyEvent( SDL_Scancode pKey , KeyState pState )
{
	mState = pState;
	mKey = pKey;
	mMouse = false;
}

KeyEvent::~KeyEvent()
{
}
