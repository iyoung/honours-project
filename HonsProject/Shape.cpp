#include "Shape.h"


Shape::Shape()
{
}


Shape::~Shape()
{
}

Sphere::Sphere()
{
}

Sphere::~Sphere()
{
}

Cuboid::Cuboid()
{
}

void Cuboid::setDimensions(float pHeight, float pWidth, float pLength)
{
	mHeight = pHeight;
	mWidth = pWidth;
	mLength = pLength;
	mVolume = mLength*mWidth*mHeight;
	//calculate vertices of OBB based on dimensions
	mVerts.push_back(glm::vec3(-mWidth*0.5f, -mHeight*0.5f, -mLength*0.5f)); //back bottom left
	mVerts.push_back(glm::vec3(-mWidth*0.5f, -mHeight*0.5f, mLength*0.5f));  //front bottom left
	mVerts.push_back(glm::vec3(mWidth*0.5f, -mHeight*0.5f, -mLength*0.5f));  //back bottom right
	mVerts.push_back(glm::vec3(mWidth*0.5f, -mHeight*0.5f, mLength*0.5f));   //front bottom right
	mVerts.push_back(glm::vec3(-mWidth*0.5f, mHeight*0.5f, -mLength*0.5f));  //back top left
	mVerts.push_back(glm::vec3(-mWidth*0.5f, mHeight*0.5f, mLength*0.5f));   //front top left
	mVerts.push_back(glm::vec3(mWidth*0.5f, mHeight*0.5f, -mLength*0.5f));   //back top right
	mVerts.push_back(glm::vec3(mWidth*0.5f, mHeight*0.5f, mLength*0.5f));    //front top right

	mLocalNorms.push_back(glm::normalize(mVerts[1] - mVerts[0])); // (0,0,1)
	mLocalNorms.push_back(glm::normalize(mVerts[3] - mVerts[1])); // (1,0,0)
	mLocalNorms.push_back(glm::normalize(mVerts[7] - mVerts[3])); // (0,1,0)
}


Cuboid::~Cuboid()
{
}

void Plane::setDimensions(float pWidth, float pLength)
{
	mWidth = pWidth;
	mLength = pLength;
	mVerts.push_back(glm::vec3(-mWidth, 0.0f, mLength)); //front left
	mVerts.push_back(glm::vec3(-mWidth, 0.0f, -mLength)); //back left
	mVerts.push_back(glm::vec3( mWidth, 0.0f, -mLength)); //back right
	mVerts.push_back(glm::vec3( mWidth, 0.0f, mLength)); //front right

	glm::vec3 edge0(glm::normalize(mVerts[1] - mVerts[0]));
	glm::vec3 edge1(glm::normalize(mVerts[2] - mVerts[1]));

	normal = glm::vec3(glm::cross(edge1, edge0));
}
