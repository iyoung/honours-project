#include "MultiCorePhysics.h"
#include "Body.h"
#include <algorithm>
#include "Entity.h"
#include "PairManager.h"
#include "CollisionEvent.h"
#include "Material.h"
#include "PhysCollisionTask.h"

MultiCorePhysics::MultiCorePhysics()
{

}

void MultiCorePhysics::update( const float & p_DeltaTime , Application & pApp )
{
	size_t i = 0;
	SDLTaskManager* pool = SDLTaskManager::TaskManagerInstance();
	if (!mCollisions.empty())
	{
		SDL_LockMutex(pool->mLock);
		pool->taskCounter = 0;
		for (; i < mCollisions.size(); i++)
		{
			pool->addTask(new PhysCollisionTask(&mCollisions[i]));
		}
		SDL_UnlockMutex(pool->mLock);
		if (pool->hasTasks())
		{
			SDL_LockMutex(pool->mLock);
			pool->start();
			SDL_UnlockMutex(pool->mLock);
			int tasks = pool->taskCounter;
			while (tasks > 0)
			{
				Task* task = nullptr;
				SDL_LockMutex(pool->mLock);
				unsigned int size = pool->mCurrentTasks.size();
				if (size > 0)
				{
					task = pool->mCurrentTasks.front();
					pool->mCurrentTasks.pop_front();
				}
				else
				{
					tasks = pool->taskCounter;
					pool->mRunning = false;
					SDL_UnlockMutex(pool->mLock);
					continue;

				}
				if (task != nullptr)
				{
					SDL_UnlockMutex(pool->mLock);
					task->execute();
				}
				else
				{
					pool->mRunning = false;
					tasks = pool->taskCounter;
					SDL_UnlockMutex(pool->mLock);
					continue;
				}

				SDL_LockMutex(pool->mLock);
				pool->taskCounter--;
				pool->addCompleteTask(task);
				tasks = pool->taskCounter;
				SDL_UnlockMutex(pool->mLock);
				task = nullptr;
			}

			SDL_LockMutex(pool->mLock);
			pool->flushCompleteTasks();
			SDL_UnlockMutex(pool->mLock);
		}
	}

	i = 0;
	size_t size = mBodies.size();
	for ( ; i < size; i++ )
	{
		if ( mBodies[ i ]->getOwner()->isActive() && mBodies[ i ]->getBodyType() == BODY_DYNAMIC && mBodies[ i ]->isAwake() )
		{
			resolveLinearImpulses( mBodies[ i ] , p_DeltaTime );
			resolveAngularImpulses( mBodies[ i ] , p_DeltaTime );
		}
	}
	mCollisions.clear();
}


void MultiCorePhysics::initWorld( const glm::vec3 & pGravity, float pTimeStep)
{
	mGravity = pGravity;
	mTimeStep = pTimeStep;
}

void MultiCorePhysics::setPairManager( std::shared_ptr<PairManager> pManager )
{
	mPairManager = pManager;
}

void MultiCorePhysics::addEntity( Entity * pEntity )
{
	mBodies.push_back( pEntity->getBody() );
}

void MultiCorePhysics::handleEvent( Event & pEvent )
{
	if (pEvent.getEventType() == EVENT_COLLISION || pEvent.getEventType() == EVENT_COLLISION_BEGIN)
	{
		Event* E = &pEvent;
		CollisionEvent* e = (CollisionEvent*)E;
		CollisionData d = e->getData();
		glm::vec3 normal = d.mNormal;
		Body* A = d.A;
		Body* B = d.B;
		//calculate relative velocity of the two bodies and the amount of this along the collision normal
		glm::vec3 relativeVel = B->getVelocity() - A->getVelocity();
		float velDotNorm = glm::dot(relativeVel, normal);
		//if the value is positive, then the objects are moving away from one another, so don't calculate anything.
		if (velDotNorm >= 0.0f)
			return;
		//calculate coefficient of restitution
		float rest = std::min(A->getMaterial()->getRestitution(), B->getMaterial()->getRestitution());
		//calculate the normal force magnitude scalar
		float j = -(1.0f + rest) * velDotNorm;

		glm::vec3 impulse;
		float combinedInverseMass;
		if (A->getBodyType() == BODY_STATIC)
		{
			combinedInverseMass = B->getInverseMass();
		}
		else if (B->getBodyType() == BODY_STATIC)
		{
			combinedInverseMass = A->getInverseMass();
		}
		else
		{
			combinedInverseMass = A->getInverseMass() + B->getInverseMass();
		}
		j /= combinedInverseMass;

		impulse = j * normal;
		glm::vec3 tD(0.0f);
		//Compute tangential relative motion for friction calculations
		glm::vec3 tV = relativeVel - normal*velDotNorm;
		if (glm::length(tV) > 0)
		{
			tD = glm::normalize(tV);
		}
		//calculate the direction of the frictional force

		//now we have the direction of the frictional force, we need to calculate the magnitude.
		//Ff = Fk*FN
		float uA = A->getMaterial()->getKineticFriction();
		float uB = B->getMaterial()->getKineticFriction();
		float Jt = glm::max(uA, uB)*velDotNorm / combinedInverseMass;
		glm::vec3 Ff = tD*Jt;
		glm::vec3 netforce = (Ff)+(impulse);
		if (glm::dot(netforce, netforce) > 0.5f)
		{
			A->wakeup();
			B->wakeup();
		}
		//Apply collision impulses
		
		
		//calculate angular effects here

		glm::vec3 c2a = d.mContactPointA - A->getOwner()->getPosition();
		glm::vec3 c2aperpPoint = glm::dot(A->getOwner()->getPosition(), netforce)*glm::normalize(netforce);
		glm::vec3 c2aperp = d.mContactPointA - A->getOwner()->getPosition();
		glm::vec3 torqueA = glm::cross((-Ff) + (-impulse), c2aperp);
		glm::vec3 c2b = d.mContactPointB - B->getOwner()->getPosition();

		glm::vec3 c2bperpPoint = glm::dot(B->getOwner()->getPosition(), netforce)*glm::normalize(netforce);
		glm::vec3 c2bperp = d.mContactPointA - B->getOwner()->getPosition();
		glm::vec3 torqueB = glm::cross((Ff)+(impulse), c2bperp);
		

		//this code accounts for floating point errors which accumulate due to Floating point error IEEE754
		float slop = 0.01f;
		glm::vec3 correction = glm::max(d.mPenetrationDepth - slop, 0.0f) / combinedInverseMass * 0.3f * normal;
		SDL_LockMutex(A->getMutex());
		if (A->getBodyType() == BODY_DYNAMIC)
		{
			A->getOwner()->setPosition(A->getOwner()->getPosition() - correction*A->getInverseMass());
		}
		A->setAngularImpulse(torqueA);
		A->setLinearImpulse(-netforce);
		SDL_UnlockMutex(A->getMutex());
		SDL_LockMutex(B->getMutex());
		if (B->getBodyType() == BODY_DYNAMIC)
		{
			B->getOwner()->setPosition(B->getOwner()->getPosition() + correction*B->getInverseMass());
		}
		B->setLinearImpulse(netforce);
		B->setAngularImpulse(torqueB);
		SDL_UnlockMutex(B->getMutex());
	}
}

void MultiCorePhysics::shutDown()
{
	mBodies.clear();
}

MultiCorePhysics::~MultiCorePhysics()
{
	mBodies.clear();
}


void MultiCorePhysics::resolveImpulses(float pDeltaTimeS)
{
	for ( size_t i = 0; i < mBodies.size(); i++ )
	{
		if (mBodies[i]->getBodyType() == BODY_DYNAMIC && mBodies[i]->isAwake())
		{
			resolveLinearImpulses(mBodies[i], mTimeStep);
			resolveAngularImpulses(mBodies[i], mTimeStep);
		}
	}
}

void MultiCorePhysics::resolveLinearImpulses( Body* pBody, float pDeltaTimeS )
{
	pBody->setLinearImpulse( ( mGravity * pBody->getMass() ) * pDeltaTimeS );
	pBody->setLinearImpulse( pBody->getVelocity() * pBody->getDamping());

	//  fudge the aerodymanics and apply air resistance
}

void MultiCorePhysics::resolveAngularImpulses( Body* pBody, float pDeltaTimeS )
{
	pBody->setAngularImpulse(-pBody->getAngularVelocity()*pBody->getDamping());
}
