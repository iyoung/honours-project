#pragma once
#include "BroadPhase.h"
#include <vector>
#include "Body.h"
#include "Entity.h"
#include <iostream>
#include "CollisionTypes.h"




class SAPSingleCore :
	public BroadPhase
{
public:
	SAPSingleCore();
	virtual void update( Application& pApp ) ;
	virtual void addEntity( Entity* pEntity ) ;
	virtual void removeEntity( Entity* pEntity );
	virtual void setPairManager( std::shared_ptr<PairManager> pManager );
	virtual void init() { ; }
	virtual void cleanUp();
	virtual void setBatchInsertMode() { mBatchInsert = true; }
	virtual void endBatchInsertMode();
	static void updateAABB(SAP_AABB& p_AABB);
	static int comp_AABB(const void* a, const void* b);
	virtual ~SAPSingleCore();
protected:
	
	bool AABBTest(SAP_AABB& A, SAP_AABB& B);
	std::vector<SAP_AABB> m_AABBs;
	std::shared_ptr<PairManager> mPairManager;
	std::vector<Body*> mBodies;
	bool mBatchInsert;
};


