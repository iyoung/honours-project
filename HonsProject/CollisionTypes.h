#pragma once
class Body;
struct SAP_AABB
{
	float m_MaxExtents[3];
	float m_MinExtents[3];
	Body* m_Body;
	unsigned int m_ID;
	bool operator == (SAP_AABB& p_Other)
	{
		return (m_ID == p_Other.m_ID);
	}
};
enum SphereCubeCollisionType
{
	SpherePoint = 0,
	SphereEdge = 1,
	SphereFace = 2,
	Containment = 3
};