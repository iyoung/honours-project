#pragma once
#include <memory>
class Application;
class PairManager;
class Entity;

class BroadPhase
{
public:
	BroadPhase();
	virtual void init() = 0;
	virtual void update( Application& pApp ) = 0;
	virtual void addEntity( Entity* pEntity ) = 0;
	virtual void removeEntity( Entity* pEntity ) = 0;
	virtual void setPairManager( std::shared_ptr<PairManager> pManager ) = 0;
	virtual void cleanUp() = 0;
	virtual void setBatchInsertMode() = 0;
	virtual void endBatchInsertMode() = 0;
	virtual ~BroadPhase();
};

