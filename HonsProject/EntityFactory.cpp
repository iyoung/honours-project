#include "EntityFactory.h"
#include <GL\glew.h>
#include <glm\glm.hpp>
#include "FileIO.h"
#include "Entity.h"
#include "Material.h"
#include "Shape.h"
EntityFactory::EntityFactory( int numEntities )
{
	mEntities.resize( numEntities );
	mNumCubes = 0;
	mSphere = nullptr;
	mPlane = nullptr;
}

void EntityFactory::init()
{

	std::vector<glm::vec3> cubeVerts;
	std::vector<glm::vec3> cubeNormals;
	std::vector<glm::vec2> cubeUVs;
	if ( FILEIO::FileIO::loadOBJ( "cube.obj" , cubeVerts , cubeNormals , cubeUVs ) )
	{
		mCubeMesh = 0;
		cubeVBO[ 0 ] = 0;
		cubeVBO[ 1 ] = 0;
		cubeVBO[ 2 ] = 0;
		//send mesh data to GPU
		glGenVertexArrays( 1 , &mCubeMesh );
		glBindVertexArray( mCubeMesh );
		GLenum err = glGetError();
		glGenBuffers( 3 , cubeVBO );
		glBindBuffer( GL_ARRAY_BUFFER , cubeVBO[ 0 ] );//vertices
		glBufferData( GL_ARRAY_BUFFER , sizeof( glm::vec3 ) * cubeVerts.size() , cubeVerts.data() , GL_STATIC_DRAW );
		glVertexAttribPointer( ( GLuint )0 , 3 , GL_FLOAT , GL_FALSE , 0 , 0 );
		glEnableVertexAttribArray( 0 );
		glBindBuffer( GL_ARRAY_BUFFER , cubeVBO[ 1 ] );//normals
		glBufferData( GL_ARRAY_BUFFER , sizeof( glm::vec3 ) * cubeNormals.size() , cubeNormals.data() , GL_STATIC_DRAW );
		glVertexAttribPointer( ( GLuint )1 , 3 , GL_FLOAT , GL_FALSE , 0 , 0 );
		glEnableVertexAttribArray( 1 );
		glBindBuffer( GL_ARRAY_BUFFER , cubeVBO[ 2 ] );//uvs
		glBufferData( GL_ARRAY_BUFFER , sizeof( glm::vec2 ) * cubeUVs.size() , cubeUVs.data() , GL_STATIC_DRAW );
		glVertexAttribPointer( ( GLuint )2 , 2 , GL_FLOAT , GL_FALSE , 0 , 0 );
		glEnableVertexAttribArray( 2 );
		glBindVertexArray( 0 );
	}
	numCubeVerts = cubeVerts.size();
	cubeVerts.clear();
	cubeNormals.clear();
	cubeUVs.clear();
	//now generate sphere
	if ( FILEIO::FileIO::loadOBJ( "sphere.obj" , cubeVerts , cubeNormals , cubeUVs ) )
	{
		mSphereMesh = 0;
		mSphereVBO[ 0 ] = 0;
		mSphereVBO[ 1 ] = 0;
		mSphereVBO[ 2 ] = 0;
		//send mesh data to GPU
		glGenVertexArrays( 1 , &mSphereMesh );
		glBindVertexArray( mSphereMesh );
		glGenBuffers( 3 , mSphereVBO );
		glBindBuffer( GL_ARRAY_BUFFER , mSphereVBO[ 0 ] );//vertices
		glBufferData( GL_ARRAY_BUFFER , sizeof( glm::vec3 ) * cubeVerts.size() , cubeVerts.data() , GL_STATIC_DRAW );
		glVertexAttribPointer( ( GLuint )0 , 3 , GL_FLOAT , GL_FALSE , 0 , 0 );
		glEnableVertexAttribArray( 0 );
		glBindBuffer( GL_ARRAY_BUFFER , mSphereVBO[ 1 ] );//normals
		glBufferData( GL_ARRAY_BUFFER , sizeof( glm::vec3 ) * cubeNormals.size() , cubeNormals.data() , GL_STATIC_DRAW );
		glVertexAttribPointer( ( GLuint )1 , 3 , GL_FLOAT , GL_FALSE , 0 , 0 );
		glEnableVertexAttribArray( 1 );
		glBindBuffer( GL_ARRAY_BUFFER , mSphereVBO[ 2 ] );//uvs
		glBufferData( GL_ARRAY_BUFFER , sizeof( glm::vec2 ) * cubeUVs.size() , cubeUVs.data() , GL_STATIC_DRAW );
		glVertexAttribPointer( ( GLuint )2 , 2 , GL_FLOAT , GL_FALSE , 0 , 0 );
		glEnableVertexAttribArray( 2 );
		glBindVertexArray( 0 );
	}
	numSphereVerts = cubeVerts.size();
	cubeVerts.clear();
	cubeNormals.clear();
	cubeUVs.clear();

	if ( FILEIO::FileIO::loadOBJ( "plane.obj" , cubeVerts , cubeNormals , cubeUVs ) )
	{
		mPlaneMesh = 0;
		mPlaneVBO[ 0 ] = 0;
		mPlaneVBO[ 1 ] = 0;
		mPlaneVBO[ 2 ] = 0;
		//send mesh data to GPU

		glGenVertexArrays( 1 , &mPlaneMesh );
		glBindVertexArray( mPlaneMesh );
		glGenBuffers( 3 , mPlaneVBO );
		glBindBuffer( GL_ARRAY_BUFFER , mPlaneVBO[ 0 ] );//vertices
		glBufferData( GL_ARRAY_BUFFER , sizeof( glm::vec3 ) * cubeVerts.size() , cubeVerts.data() , GL_STATIC_DRAW );
		glVertexAttribPointer( ( GLuint )0 , 3 , GL_FLOAT , GL_FALSE , 0 , 0 );
		glEnableVertexAttribArray( 0 );
		glBindBuffer( GL_ARRAY_BUFFER , mPlaneVBO[ 1 ] );//normals
		glBufferData( GL_ARRAY_BUFFER , sizeof( glm::vec3 ) * cubeNormals.size() , cubeNormals.data() , GL_STATIC_DRAW );
		glVertexAttribPointer( ( GLuint )1 , 3 , GL_FLOAT , GL_FALSE , 0 , 0 );
		glEnableVertexAttribArray( 1 );
		glBindBuffer( GL_ARRAY_BUFFER , mPlaneVBO[ 2 ] );//uvs
		glBufferData( GL_ARRAY_BUFFER , sizeof( glm::vec2 ) * cubeUVs.size() , cubeUVs.data() , GL_STATIC_DRAW );
		glVertexAttribPointer( ( GLuint )2 , 2 , GL_FLOAT , GL_FALSE , 0 , 0 );
		glEnableVertexAttribArray( 2 );
		glBindVertexArray( 0 );
		
	}
	mNumPlaneverts = cubeVerts.size();
	cubeVerts.clear();
	cubeNormals.clear();
	cubeUVs.clear();
}

Entity * EntityFactory::getCube()
{
	
	if ( mNumCubes < mEntities.size() + 1 )
	{
		//generate instance of Entity class
		
		mEntities[ mNumCubes ] = new Entity();
		//generate instance of  body, material, appearance, with Cube mesh
		auto a = new Appearance();
		auto b = new Body();
		auto m = new Material();
		auto s = new Cuboid();
		mEntities[ mNumCubes ]->setActive( true );
		mEntities[ mNumCubes ]->setID( mNumCubes );

		//set appearance datas
		a->setOwner( mEntities[ mNumCubes ] );
		a->setMeshHandle( mCubeMesh );
		a->setVertexCount( numCubeVerts );
		a->setColour( glm::vec4( 1.0f , 1.0f , 0.0f, 1.0f ) );
		mEntities[ mNumCubes ]->setAppearance( a );
		//set shape data
		s->setOwner( b );
		s->setDimensions( 2.0f ,2.0f , 2.0f );
		//set material data
		m->setOwner(b);
		m->setDensity(1.0f);
		m->setKineticFriction(0.75f);
		m->setStaticFriction(0.6f);
		m->setRestitution(0.6f);
		//set body data
		b->setOwner( mEntities[ mNumCubes ] );
		b->setID( mNumCubes );
		b->setMaterial(m);
		b->setShape( s );
		b->setDamping( 0.025f );

		b->setBodyType( BODY_DYNAMIC );
		b->setInitialState();
		mEntities[ mNumCubes ]->setBody( b );

		mNumCubes++;

		return mEntities[ mNumCubes - 1 ];
	}

	return nullptr;
}

Entity * EntityFactory::getSphere()
{

	
	if ( mSphere == nullptr )
	{
		//generate instance of Entity class
		mSphere = new Entity();
		//generate instance of  body, material, appearance, with sphere mesh
		auto a = new Appearance();
		auto b = new Body();
		auto m = new Material();
		auto s = new Sphere();
		mSphere->setActive( true );
		mSphere->setID( 1001 );

		//set appearance data
		a->setOwner( mSphere );
		a->setMeshHandle( mSphereMesh );
		a->setVertexCount( numSphereVerts );
		a->setColour( glm::vec4( 0.0f , 1.0f , 0.0f , 1.0f ) );
		mSphere->setAppearance( a );
		//set shape data
		s->setOwner( b );
		s->setRadius( 1.0f);
		//set material data
		m->setOwner(b);
		m->setDensity(1.0f);
		m->setKineticFriction(0.75f);
		m->setStaticFriction(0.9f);
		m->setRestitution(0.6f);
		//set body data
		b->setOwner( mSphere );
		b->setID( mEntities.size() + 1 );
		b->setMaterial(m);
		b->setShape( s );
		b->setDamping( 0.025f );
		b->setBodyType( BODY_DYNAMIC );
		b->setInitialState();
		mSphere->setBody( b );


		return mSphere;
	}
	
	return nullptr;
}

Entity* EntityFactory::getGroundPlane()
{
	if ( mPlane == nullptr )
	{
		//generate instance of Entity class
		mPlane = new Entity();
		//generate instance of  body, material, appearance, with sphere mesh
		auto a = new Appearance();
		auto b = new Body();
		auto m = new Material();
		auto s = new Plane();
		mPlane->setActive( true );
		mPlane->setID( mEntities.size() + 2 );

		//set appearance data
		a->setOwner( mPlane );
		a->setMeshHandle( mPlaneMesh );
		a->setVertexCount( mNumPlaneverts );
		a->setColour( glm::vec4( 1.0f , 1.0f , 1.0f , 1.0f ) );
		mPlane->setAppearance( a );
		//set shape data
		s->setOwner( b );
		s->setDimensions( 10000.0f , 10000.0f );
		
		//set body data
		b->setOwner( mPlane );
		b->setID( 1024 );
		b->setMaterial(m);
		b->setShape( s );
		b->setDamping( 0.025f );
		b->setBodyType( BODY_STATIC );
		m->setOwner(b);
		m->setDensity(1.0f);
		m->setKineticFriction(0.5f);
		m->setStaticFriction(0.6f);
		m->setRestitution(0.95f);
		b->setInitialState();
		mPlane->setBody( b );
		//set material data


		return mPlane;
	}
	return nullptr;
}

void EntityFactory::cleanUp()
{
	delete mSphere;
	delete mPlane;
	if ( !mEntities.empty() )
	{
		for ( unsigned int i = 0; i < mEntities.size(); i++ )
		{
			delete mEntities[ i ];
		}
	}
	mEntities.clear();
	glDeleteBuffers( 3 , cubeVBO );
	glDeleteBuffers( 3 , mSphereVBO );
	glDeleteBuffers( 3 , mPlaneVBO );
	glDeleteVertexArrays( 1 , &mCubeMesh );
	glDeleteVertexArrays( 1 , &mSphereMesh );
	glDeleteVertexArrays( 1 , &mPlaneMesh );
}

EntityFactory::~EntityFactory()
{

}
