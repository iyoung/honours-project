#pragma once
#include <GL\glew.h>
#include <SDL.h>
#include <vector>
#include "Entity.h"
#include "Shader.h"
#include "Light.h"
#include "Camera.h"

enum RendererError
{
	CONTEXT_ERROR=0,
	WINDOW_ERROR,
	SHADER_ERROR,
	RENDER_ERROR,
	SDL_VIDEO_ERROR,
	GLEW_ERROR,
	GFX_NO_ERROR
};

class GLRenderer
{
public:
	GLRenderer();
	RendererError init( int pScreenWidth , int pScreenHeight );
	void addEntity( Entity* pEntity );
	void addLight( Light* pLight ){ mLight = pLight; }
	void addCamera( Camera* pCam ){ mCamera = pCam; }
	RendererError renderScene();
	void shutDown();
	~GLRenderer();
private:
	void RenderEntity( std::vector<Entity*>::iterator iter );
	SDL_Window		*mWindow;
	SDL_GLContext	mContext;
	std::vector<Entity*> mEntities;
	Shader* mShader;
	Light* mLight;
	Camera* mCamera;
};

