#pragma once
#include "SDLTaskManager.h"
struct SAP_AABB;
class AABBUpdatetask :
	public Task
{
public:
	AABBUpdatetask(SAP_AABB* pBox = nullptr);
	virtual void setData(void* pData);
	virtual void execute();
	virtual void* getResults();
	virtual bool getCompleteStatus();
	virtual ~AABBUpdatetask();
private:
	SAP_AABB* box;
	bool status;
};

