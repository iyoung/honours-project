#pragma once
#include <glm\glm.hpp>
#include <glm\gtx\quaternion.hpp>
#include <SDL_thread.h>
class Entity;
class Shape;
class Material;

enum BodyType
{
	BODY_STATIC=0,
	BODY_KINEMATIC=1,
	BODY_DYNAMIC=2
};
class Body
{
public:
	Body();
	void setOwner(Entity* pOwner) { mOwner = pOwner; }
	void setInitialState();
	void setMaterial(Material* pMaterial);
	void setShape(Shape* pShape);
	Entity* getOwner() { return mOwner; }
	Material* getMaterial() { return mMaterial; }
	Shape* getShape() { return mShape; }
	glm::vec3 getVelocity() { return mVelocity; }
	glm::vec3 getAngularVelocity() { return mAngularVelocity; }
	float getMass() { return mMass; }
	float getInverseMass() { return mInverseMass; }
	bool isAwake() { return mAwake; }
	void wakeup() { mAwake = true; }
	void sleep() { mAwake = false; }
	void setID(unsigned int pID) { mID = pID; }
	unsigned int getID() { return mID; }
	void setDamping(float pDamping) { mDamping = pDamping; }
	void setBodyType(BodyType pBodyType) { mBodyType = pBodyType; }
	BodyType getBodyType() { return mBodyType; }
	float getDamping() { return mDamping; }
	void setLinearImpulse(glm::vec3 pImpulse);
	void setAngularVelocity(glm::vec3 pNewAngularV);
	void setAngularImpulse(glm::vec3 pImpulse);
	void setVelocity(glm::vec3& pVel) { mVelocity = pVel; }
	SDL_mutex* getMutex() { return mLock; }
	~Body();
private:
	Entity* mOwner; //the entity which owns this body
	Shape* mShape;
	Material* mMaterial;
	float mMass; //mass calculated by evaluating density and volume, measured in kg
	float mInverseMass; // 1.0/mass, used for speeding up impulse calculations
	glm::vec3 mVelocity;  //velocity measured in m/s
	glm::vec3 mAngularVelocity;
	bool mAwake; //simple switch. if angular & linear velocities are below a threshold, this switches off the Body, preventing further movement.
	float mDamping;
	unsigned int mID;
	BodyType mBodyType;
	glm::mat3 mInverseInertiaTensor;
	SDL_mutex* mLock;
};

