#include "GLRenderer.h"
#include <iostream>
#include <algorithm>
#include <glm\glm.hpp>
GLRenderer::GLRenderer()
{
}

RendererError GLRenderer::init( int pScreenWidth , int pScreenHeight )
{
	if ( SDL_Init( SDL_INIT_VIDEO ) < 0 ) // Initialize video
		return SDL_VIDEO_ERROR;

	// Request an OpenGL 3.0 context.
	SDL_GL_SetAttribute( SDL_GL_CONTEXT_MAJOR_VERSION , 3 );
	SDL_GL_SetAttribute( SDL_GL_CONTEXT_MINOR_VERSION , 3 );
	SDL_GL_SetAttribute( SDL_GL_CONTEXT_PROFILE_MASK , SDL_GL_CONTEXT_PROFILE_CORE );
	SDL_GL_SetAttribute( SDL_GL_DOUBLEBUFFER , 1 );  // double buffering on
	SDL_GL_SetAttribute( SDL_GL_ALPHA_SIZE , 8 ); // 8 bit alpha buffering
	SDL_GL_SetAttribute( SDL_GL_MULTISAMPLEBUFFERS , 1 );
	SDL_GL_SetAttribute( SDL_GL_MULTISAMPLESAMPLES , 4 ); // Turn on x4 multisampling anti-aliasing (MSAA)
	// Create 800x600 window
	mWindow = SDL_CreateWindow( "Physics Simulation" , SDL_WINDOWPOS_CENTERED , SDL_WINDOWPOS_CENTERED ,
		pScreenWidth , pScreenHeight , SDL_WINDOW_OPENGL | SDL_WINDOW_SHOWN );
	if ( !mWindow ) // Check window was created OK
	{

		return WINDOW_ERROR;
	}
		


	mContext = SDL_GL_CreateContext( mWindow ); // Create opengl context and attach to window
	SDL_GL_SetSwapInterval( 1 ); // set swap buffers to sync with monitor's vertical refresh rate

	glEnable( GL_DEPTH_TEST );
	GLenum err = glGetError();
	if ( err != GL_NO_ERROR )
		printf( "Error: %s\n" , glewGetErrorString( err ) );
	glDepthFunc( GL_LEQUAL );
	glEnable( GL_BLEND );
	err = glGetError();
	if ( err != GL_NO_ERROR )
		printf( "Error: %s\n" , glewGetErrorString( err ) );
	glBlendFunc( GL_SRC_ALPHA , GL_ONE_MINUS_SRC_ALPHA );
	err = glGetError();
	if ( err != GL_NO_ERROR )
		printf( "Error: %s\n" , glewGetErrorString( err ) );
	glEnable( GL_CULL_FACE );
	err = glGetError();
	if ( err != GL_NO_ERROR )
		printf( "Error: %s\n" , glewGetErrorString( err ) );
	glCullFace( GL_BACK );
	err = glGetError();
	if ( err != GL_NO_ERROR )
		printf( "Error: %s\n" , glewGetErrorString( err ) );

	//this is needed for windows 
	glewExperimental = GL_TRUE;
	//initialise glew
	 err = glewInit();
	if ( GLEW_OK != err ) { // glewInit failed, something is seriously wrong
		SDL_GL_DeleteContext( mContext );
		SDL_DestroyWindow( mWindow );
		return GLEW_ERROR;
	}
	//output version
	printf( ( const char* )glGetString( GL_VERSION ) );
	printf( "\n" );
	if ( err != GL_NO_ERROR )
		printf( "Error: %s\n" , glewGetErrorString( err ) );
	//enable standard opengl rendering features

	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	mShader = new Shader();
	if ( mShader->load("Shader/phong.vert" ,"Shader/phong.frag" ) < 0 )
	{
		err = glGetError();
		if ( err != GL_NO_ERROR )
			printf( "Error: %s\n" , glewGetErrorString( err ) );
		delete mShader;
		SDL_GL_DeleteContext( mContext );
		SDL_DestroyWindow( mWindow );
		return SHADER_ERROR;
	}
	return GFX_NO_ERROR;
}

RendererError GLRenderer::renderScene()
{

	glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
	mShader->activate();
	for (std::vector<Entity*>::iterator iter = mEntities.begin();iter != mEntities.end(); iter++)
	{
		RenderEntity(iter);
	}
	mShader->deactivate();
	SDL_GL_SwapWindow( mWindow ); // swap buffers
	return GFX_NO_ERROR;
}
void GLRenderer::shutDown()
{
	mEntities.clear();
	SDL_GL_DeleteContext( mContext );
	if ( mWindow )
		SDL_DestroyWindow( mWindow );
}
void GLRenderer::RenderEntity( std::vector<Entity*>::iterator iter )
{
	//dereference iterator
	//get transform data
	glm::mat4 transform = ( *iter )->getTransform();
	glm::mat4 view( 1.0f ); 
	glm::mat4 projection( 1.0f );
	mCamera->getViewMatrix( view , false );
	mCamera->getProjectionMatrix( projection , true );
	glm::vec4 camPos = glm::vec4(mCamera->getPosition(),1.0f);
	//get appearance data
	auto appearance = ( *iter )->getAppearance();
	glm::vec4 lightview( mLight->getData().mPosition);
	//feed data to shader
	mShader->setGLM_Matrix( view, "viewMatrix");
	mShader->setGLM_Matrix( transform, "modelMatrix");
	mShader->setGLM_Vector( appearance->getColour() , "colour" );
	mShader->setGLM_Vector(mLight->getData().mPosition, "LightPos");
	mShader->setGLM_Vector( lightview , "eyePos" );
	mShader->setProjection( projection );
	mShader->setLight( mLight->getData() );

	glBindVertexArray( appearance->getMeshhandle() );	// Bind mesh VAO

	glDrawArrays( GL_TRIANGLES , 0 , appearance->getVertexCount() );

	glBindVertexArray( 0 );
}
void GLRenderer::addEntity( Entity* pEntity )
{
	mEntities.push_back( pEntity );
}

GLRenderer::~GLRenderer()
{

}
