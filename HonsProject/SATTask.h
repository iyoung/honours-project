#pragma once
#include "SDLTaskManager.h"
#include "PairManager.h"
#include "CollisionEvent.h"
#include <glm\glm.hpp>
class Application;
class SATTask :
	public Task
{
public:

	SATTask(PairManager* pData,Application* pApp);
	virtual void setData(void* pData);
	virtual void execute();
	virtual void* getResults();
	virtual bool getCompleteStatus();
	virtual ~SATTask();
protected:
	void sphereVSplane(Pair& pPair, CollisionEvent& e);
	void sphereVScuboid(Pair& pPair, CollisionEvent& e);
	void cuboidVSplane(Pair& pPair, CollisionEvent& e);
	void cuboidVScuboid(Pair& pPair, CollisionEvent& e);
	int computeMinProjection(glm::vec3 data[], int size, glm::vec3 axis);
	int computeMaxProjection(glm::vec3 data[], int size, glm::vec3 axis);
	int computeNearestPointToSphere(glm::vec3 data[], int size, glm::vec3 pCircleCentre, float pRadius);
	void computeNearestEdge(glm::vec3* data, int size, glm::vec3* line, glm::vec3 circleCentre);
	glm::vec3 computeContactPointSphereLine(glm::vec3 p_PointA, glm::vec3 p_PointB, glm::vec3 p_CircleCentre, float p_CircleRadius);
	glm::vec3 SphereVSplaneContact(glm::vec3 pNormal, glm::vec3 sphereCentre, float radius, float planeDepth);
	float computeMinProj(glm::vec3 data[], int size, glm::vec3 axis);
	float computeMaxProj(glm::vec3 data[], int size, glm::vec3 axis);
	int computeFarthestPoint(glm::vec3 points[], int numPoints, glm::vec3 vector);
	glm::vec3 nearestPointCircleLine(glm::vec3 pStart, glm::vec3 pEnd, glm::vec3 pCircleCentre);
	void axisCheck(float& penetration, glm::vec3& axis,glm::vec3 pointsA[], int numPointsA, glm::vec3 pointsB[], int numPointsB, glm::vec3& posA, glm::vec3& posB);
	PairManager* data;
	Application* mApp;
	CollisionEvent event;
	bool status;
};

