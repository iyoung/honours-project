#include "Profiler.h"
#include <algorithm>
#include <float.h>
Profiler* Profiler::mInstance = nullptr;
Profiler::Profiler()
{
}

Profiler * Profiler::getInstance()
{
	if ( mInstance == nullptr )
		mInstance = new Profiler();
	return mInstance;
}

void Profiler::startApplicationProfile( const std::string & pAppName )
{
	mAppTimes.mSectionName = pAppName;
	mAppTimes.mTotal = 0.0;
	mAppTimes.mWorst = 0.0;
	mAppTimes.mAverage = 0.0;
	mAppTimes.mBest = 0.0;
	mAppStart = std::chrono::system_clock::now();
}

void Profiler::startProfileSection( const std::string & pSectionName )
{
	std::vector<TimerSection>::iterator result = mAppTimes.mSubSections.begin();
	result = std::find_if( result , mAppTimes.mSubSections.end() , std::bind2nd( SectionName() , pSectionName ) );
	if ( result == mAppTimes.mSubSections.end() )
	{
		mAppTimes.mSubSections.push_back( TimerSection( pSectionName ) );
	}
	mSectionStart = std::chrono::system_clock::now();
}

bool Profiler::endProfileSection( const std::string & pSectionName )
{
	std::vector<TimerSection>::iterator result = mAppTimes.mSubSections.begin();
	result = std::find_if( result , mAppTimes.mSubSections.end() , std::bind2nd( SectionName() , pSectionName ) );
	if ( result == mAppTimes.mSubSections.end() )
		return false;
	mSectionEnd = std::chrono::system_clock::now();
	std::chrono::duration<double> elapsed = mSectionEnd - mSectionStart;
	if ( elapsed.count() < result->mBest )
		result->mBest = elapsed.count();
	else if ( elapsed.count() > result->mWorst )
		result->mWorst = elapsed.count();
	result->mTotal += elapsed.count();
	result->mProfileCount++;
}

void Profiler::startProfileSubSection( const std::string & pSectionName , const std::string & pSubSectionName )
{
	std::vector<TimerSection>::iterator result = mAppTimes.mSubSections.begin();
	result = std::find_if( result , mAppTimes.mSubSections.end() , std::bind2nd( SectionName() , pSectionName ) );
	if ( result == mAppTimes.mSubSections.end() )
	{
		startProfileSection( pSectionName );
		mSubSectionStart = std::chrono::system_clock::now();
		result = std::find_if( result , mAppTimes.mSubSections.end() , std::bind2nd( SectionName() , pSectionName ) );
	}

	std::vector<TimerSection>::iterator result1 = result->mSubSections.begin();
	result1 = std::find_if( result1 , result->mSubSections.end() , std::bind2nd( SectionName() , pSubSectionName ) );
	if ( result1 == result->mSubSections.end() )
		result->mSubSections.push_back( TimerSection( pSubSectionName ) );
	mSubSectionStart = std::chrono::system_clock::now();
}

bool Profiler::endProfileSubSection( const std::string & pSectionName , const std::string & pSubSectionName )
{
	std::vector<TimerSection>::iterator result = mAppTimes.mSubSections.begin();
	result = std::find_if( result , mAppTimes.mSubSections.end() , std::bind2nd( SectionName() , pSectionName ) );
	if ( result == mAppTimes.mSubSections.end() )
		return false;
	std::vector<TimerSection>::iterator result1 = result->mSubSections.begin();
	result1 = std::find_if( result1 , result->mSubSections.end() , std::bind2nd( SectionName() , pSubSectionName ) );
	if ( result1 == result->mSubSections.end() )
		return false;
	mSubSectionEnd = std::chrono::system_clock::now();
	std::chrono::duration<double> elapsed = mSubSectionEnd - mSubSectionStart;
	if ( elapsed.count() < result1->mBest )
		result1->mBest = elapsed.count();
	else if ( elapsed.count() > result->mWorst )
		result1->mWorst = elapsed.count();
	result1->mTotal += elapsed.count();
	result1->mProfileCount++;
	return true;
}

void Profiler::endApplicationProfile( const std::string & pFileOutput )
{
	mAppEnd = std::chrono::system_clock::now();
	std::chrono::duration<double> elapsed = mAppEnd - mAppStart;
	mAppTimes.mTotal = elapsed.count();
	mAppTimes.mProfileCount = 1;
	//output results to file. TODO
	std::fstream file;
	file.open( pFileOutput,std::fstream::out | std::fstream::trunc);
	file << mAppTimes;
	file.close();
}

Profiler::~Profiler()
{
}
