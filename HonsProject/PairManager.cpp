#include "PairManager.h"
#include <algorithm>

bool compare_by_sharedptr(const std::shared_ptr<Pair>& a, const std::shared_ptr<Pair>& b)
{
	return ((a->A->getID() == b->A->getID() && a->B->getID() == b->B->getID()) || (a->B->getID() == b->A->getID() && a->A->getID() == b->B->getID()));
}


PairManager::PairManager()
{
	mNextIndex = 0;
	mLock = SDL_CreateMutex();
	mPruneCount = 0;
}
bool PairManager::addpair( Pair& pPair )
{
	bool found = false;
	for (size_t i = 0; i < mPairs.size(); i++)
	{
		if (mPairs[i] == pPair)
		{
			return false;
		}
	}

	mPairs.push_back( pPair );
	return true;

}

Pair& PairManager::getNextPair()
{
	mNextIndex++;
	return mPairs[mNextIndex-1];
}

void PairManager::discardPair()
{
	mPruneCount++;
}

bool PairManager::hasPairs()
{
	return ( mPairs.size() > 0 && mNextIndex < mPairs.size() );
}

void PairManager::updatepairs()
{

	if ( mPairs.size() > 1 )
	{
		for ( size_t i = 0; i < mPairs.size(); i++ )
		{
			if ( !mPairs[ i ].mColliding )
				mPruneCount++;
		}
		std::partition( mPairs.begin() , mPairs.end() , ByCollide() );
		mPairs.resize( mPairs.size() - mPruneCount );
		mPruneCount = 0;
	}

	mNextIndex = 0;
}

void PairManager::clear()
{
	mPairs.clear();
}

PairManager::~PairManager()
{
	SDL_DestroyMutex(mLock);
}
