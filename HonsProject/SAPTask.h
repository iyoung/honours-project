#pragma once
#include "SDLTaskManager.h"
#include <vector>
struct SAP_AABB;
class PairManager;
struct SapData
{
	PairManager* manager;
	std::vector<SAP_AABB>* aabbs;
	size_t startIndex;
};
class SAPTask :
	public Task
{
public:
	SAPTask();
	virtual void setData(void* pData);
	virtual void execute();
	virtual void* getResults();
	virtual bool getCompleteStatus();
	virtual ~SAPTask();
private:
	bool AABBTest(SAP_AABB & A, SAP_AABB & B);
	SapData* data;
	bool status;
};

