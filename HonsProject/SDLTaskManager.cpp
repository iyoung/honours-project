#include "SDLTaskManager.h"
#include <SDL.h>
struct ThreadData
{
	long taskCount;
	unsigned int threadID;
	SDLTaskManager* pool;
	ThreadData(unsigned int pThreadID, SDLTaskManager* pPool) :taskCount(0), threadID(pThreadID), pool(pPool) { ; }
};
SDLTaskManager* SDLTaskManager::mInstance = nullptr;
int threadFunc(void * pData)
{
	ThreadData* data = (ThreadData*)pData;
	SDLTaskManager* pool = data->pool;
	Task* task = nullptr;
	SDL_mutex* mut = pool->mLock;
	while (true)
	{
		SDL_LockMutex(mut);
		while (!pool->mRunning && pool->mCurrentTasks.empty())
		{
			SDL_CondWait(pool->mConditionFlag, mut);
			if (pool->mShuttingDown)
				return 0;
		}

		SDL_UnlockMutex(mut);
		SDL_LockMutex(mut);
		unsigned int size = pool->mCurrentTasks.size();
		if (size > 0)
		{
			task = pool->mCurrentTasks.front();
			pool->mCurrentTasks.pop_front();
		}
		else
		{
			pool->mRunning = false;
			SDL_UnlockMutex(mut);
			continue;
		}
		if (task != nullptr)
		{
			data->taskCount++;
		}
		else
		{
			pool->mRunning = false;
			SDL_UnlockMutex(mut);
			continue;
		}
		SDL_UnlockMutex(mut);
		task->execute();
		SDL_LockMutex(mut);
		pool->mCompleteTasks.push_back(task);
		pool->taskCounter--;
		SDL_UnlockMutex(mut);
		task = nullptr;
	}
	return 0;
}
SDLTaskManager::SDLTaskManager() :mRunning(false), mShuttingDown(false)
{
}

SDLTaskManager * SDLTaskManager::TaskManagerInstance()
{
	if (!mInstance)
	{
		mInstance = new SDLTaskManager;
		mInstance->init();
	}
	return mInstance;
}

void SDLTaskManager::addTask(Task * pTask)
{
	mCurrentTasks.push_back(pTask);
	taskCounter++;
}

void SDLTaskManager::addCompleteTask(Task * pTask)
{
	mCompleteTasks.push_back(pTask);
}

void SDLTaskManager::start()
{
	mRunning = true;
	SDL_CondBroadcast(mConditionFlag);
}

void SDLTaskManager::stop()
{
	mRunning = false;
	SDL_CondBroadcast(mConditionFlag);
}

bool SDLTaskManager::shutdown()
{
	SDL_LockMutex(mLock);
	mShuttingDown = true;
	mRunning = false;
	SDL_UnlockMutex(mLock);
	for (size_t i = 0; i < mThreads.size(); i++)
	{
		int status;
		SDL_WaitThread(mThreads[i], &status);
		if (status != 0)
		{
			return false;
		}
	}
	return true;
}

void SDLTaskManager::init()
{
	unsigned int maxcores = SDL_GetCPUCount();
	//unsigned int maxcores = 3;
	mThreads.resize(maxcores);
	mLock = SDL_CreateMutex();
	mConditionFlag = SDL_CreateCond();
	for (size_t i = 0; i < maxcores-1; i++)
	{
		char name[32];
		sprintf(name, "pool%d", i);
		mThreadData.push_back(new ThreadData(i, this));
		mThreads[i] = SDL_CreateThread(threadFunc, name,mThreadData[i]);
	}
	printf("%d cores detected. Creating threads.\n",maxcores);

}

bool SDLTaskManager::batchComplete()
{
	return (mCurrentTasks.empty());
}

bool SDLTaskManager::shuttingDown()
{
	return mShuttingDown;
}

Task * SDLTaskManager::getNextTask()
{
	Task* t = nullptr;
	bool empty = mCurrentTasks.empty();
	if (!empty)
	{	
		t = mCurrentTasks.front();
		mCurrentTasks.pop_front();
	}
	return t;
}

bool SDLTaskManager::hasTasks()
{
	return (!mCurrentTasks.empty());
}

bool SDLTaskManager::hasCompletedTasks()
{
	return (!mCompleteTasks.empty());
}

bool SDLTaskManager::isRunning()
{
	return mRunning;
}

void SDLTaskManager::flushCompleteTasks()
{
	Task* t = nullptr;
	bool empty = mCompleteTasks.empty();
	while (!empty)
	{
		t = mCompleteTasks.front();
		mCompleteTasks.pop_front();
		delete t;
		t = nullptr;
		empty = mCompleteTasks.empty();
	}
}

Task * SDLTaskManager::getnextCompleteTask()
{
	SDL_mutexP(mLock);
	Task* t = mCompleteTasks.front();
	mCompleteTasks.pop_front();
	SDL_mutexV(mLock);
	return t;
}

void SDLTaskManager::notifyThreadWorking(bool pSwitch)
{
	if (pSwitch)
		mTasksRunning++;
	else
		mTasksRunning--;
}

bool SDLTaskManager::isWorking()
{
	return (mTasksRunning > 0);
}


SDLTaskManager::~SDLTaskManager()
{
	SDL_DestroyCond(mConditionFlag);
	SDL_DestroyMutex(mLock);
}
