#pragma once
#include "Event.h"
#include <glm\glm.hpp>
class ProjectileEvent :
	public Event
{
public:
	ProjectileEvent( glm::vec3& pPos, glm::vec3& pVel);
	virtual EventType getEventType() { return EVENT_PROJECTILE; }
	glm::vec3& getVelocity() { return mProjVel; }
	glm::vec3& getPosition() { return mProjPos; }
	virtual ~ProjectileEvent();
private:
	glm::vec3 mProjPos;
	glm::vec3 mProjVel;
};

