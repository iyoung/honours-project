#include "SAPTask.h"
#include "SAPMultiCore.h"
#include "PairManager.h"
#include <vector>
#include "Body.h"
SAPTask::SAPTask()
{
}

void SAPTask::setData(void * pData)
{
	data = (SapData*)pData;
	status = false;
}

void SAPTask::execute()
{
	size_t size = data->aabbs->size();
	std::vector<SAP_AABB> aabbs = *data->aabbs;
	size_t index1 = data->startIndex;
	for (size_t i = data->startIndex+1; i < size; i++)
	{
		if (aabbs[index1].m_MaxExtents[0] < aabbs[i].m_MinExtents[0])
			break;
		else
		{
			if (AABBTest(aabbs[index1], aabbs[i]))
			{
				SDL_mutexP(data->manager->getMutex());
				data->manager->addpair(Pair(aabbs[index1].m_Body, aabbs[i].m_Body));
				SDL_mutexV(data->manager->getMutex());
			}
		}
	}
	status = true;
}

void * SAPTask::getResults()
{
	return nullptr;
}

bool SAPTask::getCompleteStatus()
{
	return status;
}

bool SAPTask::AABBTest(SAP_AABB & A, SAP_AABB & B)
{
	for (int axis = 0; axis < 3; axis++)
	{
		if (A.m_MinExtents[axis] > B.m_MaxExtents[axis] || A.m_MaxExtents[axis] < B.m_MinExtents[axis])
			return false;
	}
	return true;
}
SAPTask::~SAPTask()
{
	delete data;
}
