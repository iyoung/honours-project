#pragma once
#include "NarrowPhase.h"
#include "PairManager.h"
#include <glm\glm.hpp>
#include "CollisionEvent.h"



class SATMultiCore :
	public NarrowPhase
{
public:
	SATMultiCore();
	virtual void update( Application& pApp );
	virtual void setPairManager( std::shared_ptr<PairManager> pManager );
	virtual void shutDown();
	virtual ~SATMultiCore();
protected:
	friend class SATTask;
	static void sphereVSplane(Pair* pPair, CollisionEvent& e);
	static void sphereVScuboid(Pair* pPair, CollisionEvent& e);
	static void cuboidVSplane(Pair* pPair, CollisionEvent& e);
	static void cuboidVScuboid(Pair* pPair, CollisionEvent& e);
	static void computeNearestEdge(glm::vec3* data, int size, glm::vec3* line, glm::vec3 circleCentre);
	static glm::vec3 SphereVSplaneContact(glm::vec3 pNormal, glm::vec3 sphereCentre, float radius, float planeDepth);
	static int computeNearestPointToSphere(glm::vec3 data[], int size, glm::vec3 pCircleCentre, float pRadius);
	static int computeFarthestPoint( glm::vec3 points[], int numPoints, glm::vec3 vector );
	static glm::vec3 computeContactPointSphereLine( glm::vec3 p_PointA , glm::vec3 p_PointB , glm::vec3 p_CircleCentre , float p_CircleRadius );
	static float PPDistanceSquared(glm::vec3& pStart, glm::vec3& pEnd);
	static bool sphereVsLine(glm::vec3 p_PointA, glm::vec3 p_PointB, glm::vec3 p_CircleCentre, float p_CircleRadius, glm::vec3& pNormal);
	static glm::vec3 nearestPointCircleLine( glm::vec3 pStart , glm::vec3 pEnd , glm::vec3 pCircleCentre );
	static int computeMinProjection(glm::vec3 data[], int size, glm::vec3 axis);
	static int computeMaxProjection(glm::vec3 data[], int size, glm::vec3 axis);
	static float computeMinProj(glm::vec3 data[], int size, glm::vec3 axis);
	static float computeMaxProj(glm::vec3 data[], int size, glm::vec3 axis);
	static bool planeSphereIntersects(glm::vec3& normal, float& planePosition, float& radius, glm::vec3& sphereCentre, float& penetration, glm::vec3& contact);
	static bool lineSphereIntersects(glm::vec3& line, glm::vec2& extents, float& radius, glm::vec3& sphereCentre, float penetration, glm::vec3& contact);
	static bool pointSphereIntersects(glm::vec3& point, float& radius, glm::vec3& sphereCentre, float& penetration);
	std::shared_ptr<PairManager> mPairManager;
};

