#pragma once
#define _USE_MATH_DEFINES
#include <math.h>
#include <vector>
#include <glm\glm.hpp>
class Body;
enum ShapeType
{
	SHAPE_SPHERE = 0 ,
	SHAPE_CUBOID = 1,
	SHAPE_PLANE = 2
};
//base class for shape objects
//Derived classes will have dimensional data (radius, height, width, length etc), which indicates it's volume, and it's cross sectional area for air resistance
//These classes are also used for collision detection
class Shape
{
public:
	Shape();
	void setOwner( Body* pBody ){ mOwner = pBody; }
	virtual ShapeType getShapeType() = 0;
	float getVolume(){ return mVolume; }
	Body* getOwner(){return mOwner;}
	virtual ~Shape();
protected:
	float mVolume;
	Body* mOwner;
};

class Sphere:public Shape
{
public:
	Sphere();
	virtual ShapeType getShapeType(){ return SHAPE_SPHERE; }
	float getRadius(){  return mRadius;  }
	void setRadius( float pRadius ) { mRadius = pRadius; mVolume = ((4.0f / 3.0f)*(float)M_PI) * (mRadius*mRadius*mRadius);}
	virtual ~Sphere();

private:
	float mRadius;
};

class Cuboid:public Shape
{
public:
	Cuboid();
	virtual ShapeType getShapeType(){ return SHAPE_CUBOID; }
	void setDimensions(float pHeight, float pWidth, float pLength);
	float getWidth(){ return mWidth; }
	float getHeight(){ return mHeight; }
	float getLength(){ return mLength; }
	std::vector<glm::vec3>& getVerts() { return mVerts; }
	std::vector<glm::vec3>& getNormals() { return mLocalNorms; }
	~Cuboid();
private:
	float mWidth , mHeight , mLength;
	std::vector<glm::vec3> mVerts;
	std::vector<glm::vec3> mLocalNorms;
};

class Plane :public Shape
{
public :
	virtual ShapeType getShapeType() { return SHAPE_PLANE; }
	void setDimensions(float pWidth, float pLength);
	float getLength() { return mLength; }
	float getWidth() { return mWidth; }
	glm::vec3& getNormal() { return normal; }
	std::vector<glm::vec3>& getVerts() { return mVerts; }
private:
	float mWidth , mLength;
	glm::vec3 normal;
	std::vector<glm::vec3> mVerts;
};





