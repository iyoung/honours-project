#include "Application.h"
#include "CollisionSolver.h"
#include "PhysicsSystem.h"
#include "SingleCorePhysicsSystem.h"
#include "MultiCorePhysics.h"
#include "TimerEvent.h"
#include "Timer.h"
#include <iostream>
#include <fstream>
#include "EventHandler.h"
#include "ProjectileEvent.h"
#include "EntityFactory.h"
#include "Profiler.h"
#include <sstream>
Application::Application() :mRunning(false)
{
	mCollisionSolver = nullptr;
	mRenderer = nullptr;
	mController = nullptr;
	mPhysicsSolver = nullptr;
	mFactory = nullptr;
	mNumCycles = 0;
}

ApplicationError Application::init(CollisionMode pMode)
{
	if ( pMode == CPU_MULTI_THREAD )
	{
		mAppName = "MultiThread_Version";
	}
	else
	{
		mAppName = "Single_Thread_version";
	}
	mCollisionSolver = new CollisionSolver();
	mCollisionSolver->init( pMode );
	mController = new Controller();
	mController->setGameMode( true );
	mRenderer = new GLRenderer();
	RendererError err = mRenderer->init( 800 , 600 );
	if ( err != GFX_NO_ERROR )
	{
		delete mCollisionSolver;
		delete mController;
		delete mRenderer;
		return GL_ERROR;
	}
	if (pMode == CPU_SINGLE_THREAD)
	{
		mPhysicsSolver = new SingleCorePhysicsSystem();
	}
	else if (pMode == CPU_MULTI_THREAD)
	{
		mPhysicsSolver = new MultiCorePhysics();
	}
	else
	{
		//mPhysicsSolver = new GPUPhysicsSystem();
	}
	mPhysicsSolver->initWorld( glm::vec3( 0.0f , -9.98f , 0.0f ), 1.0f/60.0f );
	mFactory = new EntityFactory(1000);
	mFactory->init();
	mTimer = new Timer( 60 );
	mBestTime = 0.0f;
	mAverageTime = 0.0f;
	mWorstTime = 0.0f;
	mTimeRecorded = false;
	mEventHandler = new EventHandler();
	mRunning = true;
	construcScene();
	PROFILER->startApplicationProfile( mAppName );
	return NO_ERROR;
}

ApplicationError Application::run()
{

	mTimer->initTimer();
	while ( mRunning )
	{
		SDL_Event sdlEvent;
		while ( SDL_PollEvent( &sdlEvent ))
		{
			mController->update( sdlEvent, *this );
		}
		if ( mRunning ) {
			//update timer
			mTimer->update( *this );
		}
		//if ( mNumCycles > 30000 ) //30k cycles is enough to get good data.
			//mRunning = false;
	}
	//no longer running, clean up and exit
	PROFILER->endApplicationProfile( mAppName + ".txt" );
	cleanup();

	return NO_ERROR;
}

void Application::handleEvent( Event & pEvent )
{

	if ( pEvent.getEventType() == EVENT_TIMESTEP )
	{
		mCollisionSolver->update( *this );
		Event* e = &pEvent;
		TimerEvent* t = dynamic_cast< TimerEvent* >( e );
		mPhysicsSolver->update( t->getDeltaTime() , *this );
		float processingTime = t->getDeltaTime();
		if ( mSphere != nullptr )
			mSphere->update(processingTime);
		if(!mEntities.empty() )
		{
			for ( size_t i = 0; i < mEntities.size(); i++ )
			{
				if (mEntities[i]->isActive() && mEntities[i]->getBody()->isAwake())
					mEntities[ i ]->update(processingTime);
			}
		}
		mRenderer->renderScene();

	}
	else if ( pEvent.getEventType() == EVENT_COLLISION || pEvent.getEventType() == EVENT_COLLISION_BEGIN )
	{
		//send collision event to event handler and physics system
		mEventHandler->handleEvent( pEvent , *this );
		mPhysicsSolver->handleEvent( pEvent );
	}
	else if ( pEvent.getEventType() == EVENT_PROJECTILE )
	{
		//get a projectile from the factory and place into the scene.
		auto projectile = mFactory->getSphere();
		if ( projectile != nullptr )
		{
			ProjectileEvent* p = dynamic_cast< ProjectileEvent* >( &pEvent );
			projectile->getBody()->setVelocity( p->getVelocity() );
			projectile->setPosition( p->getPosition() );
			projectile->getBody()->wakeup();
			projectile->setTransform();
			projectile->setActive(true);
			//projectile->getBody()->setAngularImpulse(glm::quat(glm::vec3(0.0f, 90.0f, 0.0f)));
			mSphere = projectile;
			mCollisionSolver->addEntity( projectile );
			mRenderer->addEntity( projectile );
			mPhysicsSolver->addEntity( projectile );
			mSphere = projectile;
		}
	}
	else if ( pEvent.getEventType() == EVENT_SHUTDOWN )
	{
		mRunning = false;
		mCollisionSolver->shutDown();
		mRenderer->shutDown();
		mPhysicsSolver->shutDown();
		mFactory->cleanUp();
	}
	else
	{
		mEventHandler->handleEvent( pEvent , *this );
	}
}

void Application::cleanup()
{
	//OutputResultsToFile();
	delete mTimer;
	delete mCollisionSolver;
	delete mPhysicsSolver;
	delete mRenderer;
	delete mController;
	delete mFactory;
	delete mEventHandler;
	//delete event handler
}

Application::~Application()
{

}

void Application::OutputResultsToFile()
{
	mAverageTime /= mNumCycles;
	std::ofstream file;
	file.open( "results.txt" , std::ios::out | std::ios::ate );
	file << "Single Core Timing results:\n";
	file << "Best Time: " << mBestTime << std::endl;
	file << "Worst Time: " << mWorstTime << std::endl;
	file << "Average Time: " << mAverageTime << std::endl;
	file << "Cycles for test: " << mNumCycles << std::endl;
	file.close();
}

void Application::construcScene()
{
	//request one plane for the ground and add to collision solver and graphics system
	auto plane = mFactory->getGroundPlane();
	plane->setPosition( glm::vec3( 0.0f ) );
	plane->setTransform();
	mCollisionSolver->addEntity( plane );
	mRenderer->addEntity( plane );

	/*request 10 X 10 X 10 cubes, add to graphics, physics and collision systems.*/
	float offset = 1.5f;
	unsigned int stackSize = 5;
	std::ostringstream convert;
	convert << ( stackSize*stackSize*stackSize );
	mAppName += "_" + convert.str();
	mCollisionSolver->setBatchMode();
	for ( unsigned int k = 0; k < stackSize; k++ )
	{
		for (unsigned int j = 0; j < stackSize; j++)
		{
			for ( unsigned int i = 0; i < stackSize;i++ )
			{
				auto cube = mFactory->getCube();
				glm::vec3 pos(offset + 2.01f*i, offset + 2.01f*j, offset + 2.01f*k);
				cube->setPosition( pos );
				cube->setTransform();
				//cube->getBody()->setAngularVelocity(glm::vec3(0.0f,1.0f,0.0f));
				mCollisionSolver->addEntity( cube );
				mRenderer->addEntity( cube );
				mPhysicsSolver->addEntity( cube );
				cube->setActive( true );
				cube->getBody()->wakeup();
				mRenderer->addEntity( cube );
				mEntities.push_back( cube );
			}
		}
	}
	mCollisionSolver->EndBatchMode();
	//create camera and add to graphics and event handler.
	auto cam = new Camera();
	cam->MoveForward( -200.0f );
	cam->MoveUp( 2.0f );
	cam->setDrawDistance( 1500.0f );
	cam->setFieldOfView( 110.0f );
	cam->setViewPort( 800 , 600 );
	mEventHandler->setCamera( cam );
	mRenderer->addCamera( cam );

	//finally, add a light
	Light* light = new Light();
	light->move( glm::vec3( 10.0f , 25.0f , -10.0f ) );
	light->setDiffuse( glm::vec4( 1.0f , 1.0f , 1.0f , 1.0f ) );
	light->setAttenuation( glm::vec3( 1.0f , 0.1f , 0.01f ) );
	mRenderer->addLight( light );

}
