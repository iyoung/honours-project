#pragma once
class Event;
class Application;
class Camera;
class EventHandler
{
public:
	EventHandler();
	void setCamera( Camera* pCam ) { mCamera = pCam; }
	void handleEvent( Event & pEvent, Application & pApp );
	~EventHandler();
private:
	Camera* mCamera;
	float mCameraMoveSpeed;
	float mCameraRotateSpeed;
	bool mProjectileFired;
};

