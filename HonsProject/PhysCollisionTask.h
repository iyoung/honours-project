#pragma once
#include "SDLTaskManager.h"
#include "CollisionEvent.h"
class PhysCollisionTask :
	public Task
{
public:
	PhysCollisionTask();
	PhysCollisionTask(CollisionData* pData);
	virtual void setData(void* pData);
	virtual void execute();
	virtual void* getResults();
	virtual bool getCompleteStatus();
	virtual ~PhysCollisionTask();
protected:
	CollisionData* data;
	bool status;
};

