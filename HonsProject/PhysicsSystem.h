#pragma once
class Application;
#include <glm\glm.hpp>
#include <memory>
class PairManager;
class Entity;
class Event;
class PhysicsSystem
{
public:
	PhysicsSystem();
	virtual void update( const float& p_DeltaTime , Application& pApp ) = 0;
	virtual void initWorld( const glm::vec3& pGravity, float pTimeStep ) = 0;
	virtual void setPairManager( std::shared_ptr<PairManager> pManager ) = 0;
	virtual void handleEvent( Event& pEvent )=0;
	virtual void addEntity( Entity* pEntity ) = 0;
	virtual void shutDown()=0;
	virtual ~PhysicsSystem();
};

