#include "Light.h"


Light::Light(void)
{
}


void Light::setDiffuse(const glm::vec4& pDiffuse)
{
    mData.mDiffuse = pDiffuse;
}

void Light::setPosition(const glm::vec4& pPosition)
{
    mData.mPosition = pPosition;
}

void Light::move(const glm::vec3& pOffset)
{
	mData.mPosition+=glm::vec4(pOffset,0.0f);
}
void Light::setAttenuation(const glm::vec3& pAttenuation)
{
	mData.mAttenuation = pAttenuation;
}

LightData& Light::getData()
{
    return mData;
}

Light::~Light(void)
{
}
