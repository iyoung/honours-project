#pragma once
#include "Event.h"
class TimerEvent :
	public Event
{
public:
	TimerEvent(float & pTimeS);
	const float& getDeltaTime() { return mDeltaTime; }
	virtual EventType getEventType() { return EVENT_TIMESTEP; }
	virtual ~TimerEvent();
private:
	float mDeltaTime;
};

