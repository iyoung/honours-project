#pragma once
#include <memory>
template <class T> class Node
{
public:
	Node(T& pElement) { mElement = pElement; }
	~Node() { ; }
	Node<T>* mNext;
	Node<T>* mPrevious;
	T mElement;
	size_t mIndex;
};
template <class T> class CircularList
{
public:
	CircularList():mCapacity(0),mSize(0),mCurrent(nullptr),mElements(nullptr) { ; }
	void push_back(T& pElement) {
		if (mCapacity == 0)
		{
			mElements = new Node<T>(pElement);
			mCurrent = mElements;
		}
		if (mSize == mCapacity)
		{
			resize(mSize * 2);
		}
		mElements[mSize] = new Node<T>();
		mElements[mSize].mElement;
		if (mSize > 1)
		{
			mElements[0].mPrevious = &mElements[mSize];
			mElements[mSize].mPrevious = &mElements[mSize - 1];
			mElements[mSize - 1].mNext = &mElements[mSize];
		}
	}
	T& back()
	{
		return mElements[mSize - 1].mElement;
	}
	T& front() 
	{
		return mElements[0].mElement;
	}
	T& operator [](size_t pIndex) 
	{
		return mElements[pIndex].mElement;
	}
	T& previous()
	{
		return mElements[((mCurrent.mIndex > 0) ? mCurrent.mIndex - 1 : mSize - 1)].mElement;
	}
	T& next() 
	{
		return mElements[((mCurrent.mIndex < mSize - 1) ? mCurrent.mIndex + 1 : 0)].mElement;
	}
	void resize(size_t pSize)
	{
		Node<T>* p = mElements;
		mElements = new Node<T>[pSize];
		if (pSize < mSize)
		{
			mElements[pSize - 1].mNext = mElements[0];
			mElements[0].mPrevious = mElements[pSize - 1];
		}
		for (size_t i = 0; i < pSize; i++)
		{
			//if resizing up
			if (mSize < pSize)
			{
				if (i < mSize)
					mElements[i] = p[i];
				else
				{
					mElements[i] = nullptr;
				}
			}
			else
			{
				//if we are resizing down
				if (i < pSize)
				{
					mElements[i] = p[i];
				}
				else
				{
					delete mElements[i];
				}
			}
		}
		mCapacity = pSize;
		if (mSize > pSize)
			mSize = pSize;
	}
	~CircularList() { ; }
private:

	Node<T>* mElements;
	Node<T> mCurrent;
	size_t mSize;
	size_t mCapacity;
};

