#pragma once
#include <glm\glm.hpp>
class Entity;
class Appearance
{
public:
	Appearance();
	void setOwner( Entity* pOwner ){ mOwner = pOwner; }
	glm::vec4 getColour(){ return mColour; }
	unsigned int getMeshhandle(){ return mMeshHandle; }
	unsigned int getVertexCount(){ return mVertexCount; }
	void setVertexCount( unsigned int pVertCount ){ mVertexCount = pVertCount; }
	void setMeshHandle( unsigned int pMeshHandle ){ mMeshHandle = pMeshHandle; }
	void setColour( glm::vec4 pColour ){ mColour = pColour; }
	~Appearance();
private:
	Entity* mOwner; //the owner of the appearance object
	glm::vec4 mColour; //colour data in rgba format
	unsigned int mMeshHandle; //opengl mesh handle
	unsigned int mVertexCount; //vertex count for mesh
};

