#pragma once
#include <vector>
class Entity;
class EntityFactory
{
public:
	EntityFactory( int numEntities = 256);
	void init();
	Entity* getCube();
	Entity* getSphere();
	Entity* getGroundPlane();
	void cleanUp();
	~EntityFactory();
private:
	std::vector<Entity*> mEntities;
	Entity* mSphere;
	Entity* mPlane;
	unsigned int mNumCubes;
	unsigned int mCubeMesh;
	unsigned int cubeVBO[ 3 ];
	unsigned int mSphereMesh;
	unsigned int mSphereVBO[ 3 ];
	unsigned int numCubeVerts;
	unsigned int numSphereVerts;
	unsigned int mPlaneMesh;
	unsigned int mPlaneVBO[ 3 ];
	unsigned int mNumPlaneverts;
};

