#pragma once
#include <vector>
#include <deque>
#include <SDL_thread.h>
struct ThreadData;
class Task
{
public:
	Task() { ; }
	virtual ~Task() { ; }
	virtual void setData(void* pData) = 0;
	virtual void execute() = 0;
	virtual void* getResults() = 0;
	virtual bool getCompleteStatus() = 0;
};

class SDLTaskManager
{
public:
	static SDLTaskManager* TaskManagerInstance();
	void addTask(Task* pTask);
	void addCompleteTask(Task* pTask);
	void start();
	void stop();
	bool shutdown();
	void init();
	bool batchComplete();
	bool shuttingDown();
	Task* getNextTask();
	bool hasTasks();
	bool hasCompletedTasks();
	bool isRunning();
	void flushCompleteTasks();
	Task* getnextCompleteTask();
	void notifyThreadWorking(bool pSwitch);
	bool isWorking();
	void incActiveTasks() { mTasksRunning++; }
	void decActiveTasks() { mTasksRunning--; }
	SDL_cond* getCondition() { return mConditionFlag; }
	SDL_mutex* getMutex() { return mLock; }
	bool mRunning;
	bool mShuttingDown;
	std::deque<Task*> mCurrentTasks;
	std::deque<Task*> mCompleteTasks;
	SDL_mutex* mLock;
	SDL_cond* mConditionFlag;
	int mTasksRunning;
	int taskCounter;
	~SDLTaskManager();
private:
	SDLTaskManager();
	static SDLTaskManager* mInstance;
	std::vector<SDL_Thread*> mThreads;
	std::vector<ThreadData*> mThreadData;
	
};



