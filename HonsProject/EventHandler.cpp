#include "EventHandler.h"
#include "CollisionEvent.h"
#include "Appearance.h"
#include "Entity.h"
#include "KeyEvent.h"
#include "Camera.h"
#include "Application.h"
#include "ProjectileEvent.h"

EventHandler::EventHandler()
{
	mCameraMoveSpeed = 1.0f;
	mCameraRotateSpeed = 0.01f;
	mProjectileFired = false;
}

void EventHandler::handleEvent( Event & pEvent , Application & pApp )
{
	if ( pEvent.getEventType() == EVENT_COLLISION || pEvent.getEventType() == EVENT_COLLISION_BEGIN )
	{
		Event* E = &pEvent;
		CollisionEvent* e = dynamic_cast< CollisionEvent* >( E );
		CollisionData p = e->getData();
		if (p.A->getBodyType() == BODY_DYNAMIC)
			p.A->getOwner()->getAppearance()->setColour( glm::vec4( 1.0f , 0.0f , 0.0f , 1.0f ) );
		if (p.B->getBodyType() == BODY_DYNAMIC)
			p.B->getOwner()->getAppearance()->setColour( glm::vec4( 1.0f , 0.0f , 0.0f , 1.0f ) );

	}
	else if ( pEvent.getEventType() == EVENT_COLLISION_END )
	{
		Event* E = &pEvent;
		CollisionEvent* e = dynamic_cast< CollisionEvent* >( E );
		CollisionData p = e->getData();
		p.A->getOwner()->getAppearance()->setColour( glm::vec4( 1.0f , 1.0f , 0.0f , 1.0f ) );
		p.B->getOwner()->getAppearance()->setColour( glm::vec4( 0.0f , 1.0f , 0.0f , 1.0f ) );
	}
	//else if keypresses
	else if ( pEvent.getEventType() == EVENT_KEYPRESS )
	{
		Event* E = &pEvent;
		KeyEvent* e = dynamic_cast< KeyEvent* >( E );
		if ( e->isMouseEvent() )
		{
			//rotate camera or fire projectile
			if ( e->getMouseButtonIndex() == SDL_BUTTON_LEFT )
			{
				if ( e->getState() == MOUSE_BUTTON_RELEASED )
				{
					//send event to place a projectile in the world
					glm::vec3 l = mCamera->getLookDirection();
					pApp.handleEvent(ProjectileEvent(mCamera->getPosition()+l*2.0f, l *100.0f));
					mProjectileFired = true;
				}
			}
			else
			{
				mCamera->RotatePitch( mCameraRotateSpeed*-e->getMouseMotionY() );
				mCamera->RotateYaw( mCameraRotateSpeed*-e->getMouseMotionX() );
			}
		}
		else
		{
			//parse key and if pressed or released.
			SDL_Scancode key = e->getKey();
			switch ( key )
			{
			case SDL_SCANCODE_W:
				mCamera->MoveForward( mCameraMoveSpeed );
				break;
			case SDL_SCANCODE_S:
				mCamera->MoveForward( -mCameraMoveSpeed );
				break;
			case SDL_SCANCODE_A:
				mCamera->MoveRight( -mCameraMoveSpeed );
				break;
			case SDL_SCANCODE_D:
				mCamera->MoveRight( mCameraMoveSpeed );
				break;
			case SDL_SCANCODE_LCTRL:
				mCamera->MoveUp(-mCameraMoveSpeed);
				break;
			case SDL_SCANCODE_SPACE:
				mCamera->MoveUp(mCameraMoveSpeed);
			default:
				break;
			}
		}

	}
}


EventHandler::~EventHandler()
{
}
