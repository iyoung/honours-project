#pragma once
#include "Body.h"
#include "Appearance.h"
#include <glm\glm.hpp>

class Entity
{
public:
	Entity();
	void addBody( Body* pBody ){ mBody = pBody; pBody->setOwner( this ); }
	void addAppearance( Appearance* pAppearance ){ mAppearance = pAppearance; }
	void setPosition(glm::vec3 pPosition);
	void update( const float& pDeltaTimeS );
	glm::vec3 getPosition(){ return mPosition; }
	Body* getBody(){ return mBody; }
	Appearance* getAppearance(){ return mAppearance; }
	glm::mat4& getTransform();
	void setID( unsigned int pID ) { mID = pID; }
	unsigned int getID() { return mID; }
	bool operator ==( Entity* pOther ) { return mID == pOther->getID(); }
	bool isActive(){ return mActive; }
	void setActive(bool pSwitch){ mActive=pSwitch; }
	void setAppearance( Appearance* pAppearance ) { mAppearance = pAppearance; }
	void setBody( Body* pBody ) { mBody = pBody; }
	bool hasMoved() { return mMoved; }
	glm::quat& getOrientation() { return mOrientation; }
	void setMoved(bool pMoved) { mMoved = pMoved; }
	void cleanup();
	void setTransform();
	~Entity();
private:

	Body* mBody;
	Appearance* mAppearance;
	glm::vec3 mPosition;
	bool mActive;
	bool mMoved;
	glm::mat4 mTransform;
	glm::quat mOrientation;
	unsigned int mID;
};

