#include "Body.h"
#include "Shape.h"
#include "Material.h"

Body::Body()
{
	mOwner = nullptr;
	mMaterial = nullptr;
	mShape = nullptr;
	mMass = 0.0f;
	mInverseMass = 0.0f;
	glm::vec3 mVelocity = glm::vec3(0.0f);
	glm::vec3 mRotationAxis = glm::vec3( 0.0f );
	mAngularVelocity = glm::vec3(0.0f);
	mAwake = false;
	mLock = SDL_CreateMutex();
}

void Body::setInitialState()
{
	if (mShape != nullptr && mMaterial !=nullptr)
	{
		mMass = mShape->getVolume()*mMaterial->getDensity();
		mInverseMass = 1.0f / mMass;
		if (mShape->getShapeType() == SHAPE_CUBOID)
		{
			Cuboid* c = (Cuboid*)mShape;
			float IX = ((c->getHeight()*c->getHeight()) + (c->getLength()*c->getLength()))*(mMass / 12.0f);
			float IY = ((c->getWidth()*c->getWidth()) + (c->getLength()*c->getLength()))*(mMass / 12.0f);
			float IZ = ((c->getHeight()*c->getHeight()) + (c->getWidth()*c->getWidth()))*(mMass / 12.0f);
			mInverseInertiaTensor = glm::mat3(1.0f);
			mInverseInertiaTensor[0][0] = IX;
			mInverseInertiaTensor[1][1] = IY;
			mInverseInertiaTensor[2][2] = IZ;
			mInverseInertiaTensor = glm::inverse(mInverseInertiaTensor);
		}
		else if (mShape->getShapeType() == SHAPE_SPHERE)
		{
			Sphere* s = (Sphere*)mShape;
			float Inertia = (2.0f* mMass * (s->getRadius() * s->getRadius()))*0.2f;//formula for a solid sphere
			mInverseInertiaTensor = glm::mat3(Inertia);
			mInverseInertiaTensor = glm::inverse(mInverseInertiaTensor);
		}
		else
		{
			Plane* p = (Plane*)mShape;
			float Ic = mMass / 12.0f*(p->getLength()*p->getLength()*p->getWidth()*p->getWidth());
			mInverseInertiaTensor = glm::mat3(Ic);
			mInverseInertiaTensor = glm::inverse(mInverseInertiaTensor);
		}
	}
}

void Body::setMaterial( Material* pMaterial )
{
	if ( mShape != nullptr )
	{
		mMass = mShape->getVolume()*pMaterial->getDensity();
		mInverseMass = 1.0f / mMass;
	}
	mMaterial = pMaterial;
	mMaterial->setOwner( this );
}

void Body::setShape( Shape* pShape )
{
	if ( mMaterial != nullptr )
	{
		mMass = pShape->getVolume()*mMaterial->getDensity();
		mInverseMass = 1.0f / mMass;
		if (pShape->getShapeType() == SHAPE_CUBOID)
		{
			Cuboid* c = (Cuboid*)pShape;
			float IX = ((c->getHeight()*c->getHeight()) + (c->getLength()*c->getLength()))*(mMass / 12.0f);
			float IY = ((c->getWidth()*c->getWidth()) + (c->getLength()*c->getLength()))*(mMass / 12.0f);
			float IZ = ((c->getHeight()*c->getHeight()) + (c->getWidth()*c->getWidth()))*(mMass / 12.0f);
			mInverseInertiaTensor = glm::mat3(1.0f);
			mInverseInertiaTensor[0][0] = IX;
			mInverseInertiaTensor[1][1] = IY;
			mInverseInertiaTensor[2][2] = IZ;
			mInverseInertiaTensor = glm::inverse(mInverseInertiaTensor);
		}
		else if (pShape->getShapeType() == SHAPE_SPHERE)
		{
			Sphere* s = (Sphere*)pShape;
			float Inertia = (2.0f* mMass * (s->getRadius() * s->getRadius()))*0.2f;//formula for a solid sphere
			mInverseInertiaTensor = glm::mat3(Inertia);
			mInverseInertiaTensor = glm::inverse(mInverseInertiaTensor);
		}
		else
		{
			Plane* p = (Plane*)pShape;
			float Ic = mMass / 12.0f*(p->getLength()*p->getLength()*p->getWidth()*p->getWidth());
			mInverseInertiaTensor = glm::mat3(Ic);
			mInverseInertiaTensor = glm::inverse(mInverseInertiaTensor);
		}
	}

	mShape = pShape;
	mShape->setOwner( this );
}

void Body::setLinearImpulse( glm::vec3 pImpulse )
{
	if (mAwake)
	{
		if (mBodyType == BODY_DYNAMIC)
		{
			mVelocity += pImpulse*mInverseMass;
		}
	}
}

void Body::setAngularVelocity( glm::vec3 pNewAngularV )
{
	mAngularVelocity = pNewAngularV;
}

void Body::setAngularImpulse( glm::vec3 pImpulse )
{
	if (mAwake)
	{
		if (mBodyType == BODY_DYNAMIC)
		{
			mAngularVelocity += mInverseInertiaTensor*pImpulse;
		}
	}
}

Body::~Body()
{
	SDL_DestroyMutex(mLock);
}
