#pragma once
#include "Body.h"
#include <vector>
#include <memory>
#include <SDL_thread.h>

struct Pair
{
public:
	Body* A;
	Body* B;
	bool mColliding;
	Pair() { ; }
	Pair( Body* A , Body* B )
	{
		this->A = A;
		this->B = B;
		mColliding = false;
	}

	bool operator== (Pair& pOther )
	{
		return ( ( this->A->getID() == pOther.A->getID() && this->B->getID() == pOther.B->getID() ) || ( this->B->getID() == pOther.A->getID() && this->A->getID() == pOther.B->getID() ) );
	}

	bool operator== (std::shared_ptr<Pair>& pOther)
	{
		return ((this->A->getID() == pOther->A->getID() && this->B->getID() == pOther->B->getID()) || (this->B->getID() == pOther->A->getID() && this->A->getID() == pOther->B->getID()));
	}

};

class ByCollide
{
public:
	bool operator()(const Pair &a) const
	{
		return ( a.mColliding );
	}
};

class PairManager
{
public:
	PairManager();
	~PairManager();
	bool addpair( Pair &pPair );
	Pair& getNextPair();
	void discardPair();
	bool hasPairs();
	int getPairCount() { return mPairs.size(); }
	SDL_mutex* getMutex() { return mLock; }
	void updatepairs();
	void clear();

private:
	SDL_mutex* mLock;
	std::vector<Pair> mPairs;
	size_t mNextIndex;
	int mPruneCount;
};

