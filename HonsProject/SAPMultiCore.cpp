#include "SAPMultiCore.h"
#include <glm\glm.hpp>
#include "Body.h"
#include "Entity.h"
#include "Shape.h"
#include <algorithm>
#include "PairManager.h"
#include <stdlib.h>
#include "SDLTaskManager.h"
#include "AABBUpdatetask.h"
#include "SAPTask.h"
#include "SAPSingleCore.h"
#include "Profiler.h"

SAPMultiCore::SAPMultiCore()
{
}

void SAPMultiCore::update( Application & pApp )
{
	PROFILER->startProfileSubSection("Multi_Thread_BroadPhase","Update_AABB_SAP");
	auto pool = SDLTaskManager::TaskManagerInstance();
	size_t size = m_AABBs.size();
	SDL_mutex* mut = pool->mLock;
	SDL_LockMutex( mut );
	for (size_t i = 0; i < size; i++)
	{
		pool->addTask(new AABBUpdatetask(&m_AABBs[i]));
	}
	pool->start();
	SDL_UnlockMutex( mut );

	int tasks = pool->taskCounter;
	while (tasks > 0)
	{
		Task* task = nullptr;
		SDL_LockMutex( mut );
		unsigned int size = pool->mCurrentTasks.size();
		if (size > 0)
		{
			task = pool->mCurrentTasks.front();
			pool->mCurrentTasks.pop_front();
		}
		else
		{
			tasks = pool->taskCounter;
			pool->mRunning = false;
			SDL_UnlockMutex( mut );
			continue;

		}
		if (task != nullptr)
		{
			SDL_UnlockMutex( mut );
			task->execute();
		}
		else
		{
			pool->mRunning = false;
			tasks = pool->taskCounter;
			SDL_UnlockMutex( mut );
			continue;
		}

		SDL_LockMutex( mut );
		pool->taskCounter--;
		pool->addCompleteTask(task);
		tasks = pool->taskCounter;
		SDL_UnlockMutex( mut );
		task = nullptr;
	}
	//we don't need results back so flush the queue
	SDL_LockMutex( mut );
	while (!pool->mCompleteTasks.empty())
	{
		Task* t = pool->mCompleteTasks.front();
		pool->mCompleteTasks.pop_front();
		delete t;
	}
	SDL_UnlockMutex( mut );
	//now AABBs are updated, sort the list
	qsort(m_AABBs.data(), m_AABBs.size(), sizeof(SAP_AABB), SAPSingleCore::comp_AABB);
	//next task set: sweep the list and generate pairs.
	PROFILER->endProfileSubSection( "Multi_Thread_BroadPhase" , "Update_AABB_SAP" );
	PROFILER->startProfileSubSection( "Multi_Thread_BroadPhase" , "Pair_Generation" );
	SDL_LockMutex( mut );
	pool->taskCounter = 0;
	if (m_AABBs.size() >= 2)
	{
		for (size_t i = 0; i < m_AABBs.size() - 1; i++)
		{
			Task* task = new SAPTask();
			SapData* data = new SapData();
			data->aabbs = &m_AABBs;
			data->manager = mPairManager.get();
			data->startIndex = i;
			task->setData(data);
			pool->addTask(task);
		}
		pool->start();
		SDL_UnlockMutex( mut );

		tasks = pool->taskCounter;
		while (tasks > 0)
		{
			Task* task = nullptr;
			SDL_LockMutex( mut );
			unsigned int size = pool->mCurrentTasks.size();
			if (size > 0)
			{
				task = pool->mCurrentTasks.front();
				pool->mCurrentTasks.pop_front();
			}
			else
			{
				tasks = pool->taskCounter;
				pool->mRunning = false;
				SDL_UnlockMutex(pool->mLock);
				continue;

			}
			if (task != nullptr)
			{
				SDL_UnlockMutex( mut );
				task->execute();
			}
			else
			{
				pool->mRunning = false;
				tasks = pool->taskCounter;
				SDL_UnlockMutex( mut );
				continue;
			}

			SDL_LockMutex( mut );
			pool->taskCounter--;
			pool->addCompleteTask(task);
			tasks = pool->taskCounter;
			SDL_UnlockMutex( mut );
			task = nullptr;
		}
		//we don't need results back so flush the queue
		SDL_LockMutex( mut );
		while (!pool->mCompleteTasks.empty())
		{
			Task* t = pool->mCompleteTasks.front();
			pool->mCompleteTasks.pop_front();
			delete t;
		}
		SDL_UnlockMutex( mut );
	}
	PROFILER->endProfileSubSection( "Multi_Thread_BroadPhase" , "Pair_Generation" );
}

void SAPMultiCore::addEntity( Entity * pEntity )
{
	//generate 6 extents (2 for each axis: 3 min, 3 max).
	//insert extents into extent vectors
	//sort all vectors into ascending order
	Body* b = pEntity->getBody();
	mBodies.push_back(b);
	m_AABBs.push_back(SAP_AABB());
	m_AABBs.back().m_Body = b;
	m_AABBs.back().m_ID = b->getID();
	
	if (!mBatchInsert && m_AABBs.size() > 1)
	{
		SAPSingleCore::updateAABB(m_AABBs.back());
		qsort(m_AABBs.data(), m_AABBs.size(), sizeof(SAP_AABB), SAPSingleCore::comp_AABB);
	}
}

void SAPMultiCore::removeEntity( Entity* pEntity )
{
	//go through each axis vector
	//and remove
	// the evaluation function

	//and update the lists

}

void SAPMultiCore::setPairManager( std::shared_ptr<PairManager> pManager )
{
	mPairManager = pManager;
}

void SAPMultiCore::cleanUp()
{
	mBodies.clear();
	m_AABBs.clear();
}

bool SAPMultiCore::AABBTest(SAP_AABB & A, SAP_AABB & B)
{
	for (int axis = 0; axis < 3; axis++)
	{
		if (A.m_MinExtents[axis] > B.m_MaxExtents[axis] || A.m_MaxExtents[axis] < B.m_MinExtents[axis])
			return false;
	}
	return true;
}

void SAPMultiCore::endBatchInsertMode()
{
	auto pool = SDLTaskManager::TaskManagerInstance();
	SDL_LockMutex(pool->mLock);
	pool->taskCounter = 0;
	for (size_t i = 0; i < m_AABBs.size(); i++)
	{
		
		pool->addTask(new AABBUpdatetask(&m_AABBs[i]));

	}
	pool->start();
	SDL_UnlockMutex(pool->mLock);
	int tasks = pool->taskCounter;
	while (tasks > 0)
	{
		Task* task = nullptr;
		SDL_LockMutex(pool->mLock);
		unsigned int size = pool->mCurrentTasks.size();
		if (size > 0)
		{
			task = pool->mCurrentTasks.front();
			pool->mCurrentTasks.pop_front();
		}
		else
		{
			tasks = pool->taskCounter;
			pool->mRunning = false;
			SDL_UnlockMutex(pool->mLock);
			continue;

		}
		if (task != nullptr)
		{
			SDL_UnlockMutex(pool->mLock);
			task->execute();
		}
		else
		{
			pool->mRunning = false;
			tasks = pool->taskCounter;
			SDL_UnlockMutex(pool->mLock);
			continue;
		}

		SDL_LockMutex(pool->mLock);
		pool->taskCounter--;
		pool->addCompleteTask(task);
		tasks = pool->taskCounter;
		SDL_UnlockMutex(pool->mLock);
		task = nullptr;
	}
	//we don't need results back so flush the queue
	SDL_LockMutex(pool->mLock);
	while (!pool->mCompleteTasks.empty())
	{
		Task* t = pool->mCompleteTasks.front();
		pool->mCompleteTasks.pop_front();
		delete t;
	}
	SDL_UnlockMutex(pool->mLock);
	qsort(m_AABBs.data(), m_AABBs.size(), sizeof(SAP_AABB), SAPSingleCore::comp_AABB);
	mBatchInsert = false; 
}

SAPMultiCore::~SAPMultiCore()
{
}