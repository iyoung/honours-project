MultiThread_Version_64: 
    Best: 0
    Worst: 0
    Average: 299.872
    Total: 299.872

Multi_Thread_BroadPhase: 
    Best: 0.0001624
    Worst: 0.0102828
    Average: 0.00103484
    Total: 18.1967

Update_AABB_SAP: 
    Best: 8.22e-05
    Worst: 0.0008981
    Average: 0.000151411
    Total: 2.6624

Pair_Generation: 
    Best: 4.96e-05
    Worst: 0.0101598
    Average: 0.000875862
    Total: 15.4012

Multi_Thread_NarrowPhase: 
    Best: 6.4e-06
    Worst: 0.0117924
    Average: 0.00157037
    Total: 27.6134

Data_Preparation: 
    Best: 1.3e-06
    Worst: 0.002602
    Average: 0.000170438
    Total: 2.99699

SAT: 
    Best: 1e-06
    Worst: 0.0107116
    Average: 0.00139348
    Total: 24.5029

