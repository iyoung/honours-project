MultiThread_Version_729: 
    Best: 0
    Worst: 0
    Average: 300.159
    Total: 300.159

Multi_Thread_BroadPhase: 
    Best: 0.0022156
    Worst: 0.701765
    Average: 0.299487
    Total: 275.528

Update_AABB_SAP: 
    Best: 0.0005365
    Worst: 0
    Average: 0.000843632
    Total: 0.776141

Pair_Generation: 
    Best: 0.0016235
    Worst: 0.700956
    Average: 0.298637
    Total: 274.746

Multi_Thread_NarrowPhase: 
    Best: 1.8e-05
    Worst: 0.0436102
    Average: 0.0198869
    Total: 18.2959

Data_Preparation: 
    Best: 1.19e-05
    Worst: 0.004411
    Average: 0.00158383
    Total: 1.45713

SAT: 
    Best: 1e-06
    Worst: 0.0408944
    Average: 0.0182964
    Total: 16.8327

