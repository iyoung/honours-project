MultiThread_Version_125: 
    Best: 0
    Worst: 0
    Average: 300.059
    Total: 300.059

Multi_Thread_BroadPhase: 
    Best: 0.0002521
    Worst: 0.0439619
    Average: 0.0156197
    Total: 196.589

Update_AABB_SAP: 
    Best: 0.0001213
    Worst: 0.0090367
    Average: 0.000214538
    Total: 2.70018

Pair_Generation: 
    Best: 0.0001158
    Worst: 0.0436904
    Average: 0.0153972
    Total: 193.789

Multi_Thread_NarrowPhase: 
    Best: 6.8e-06
    Worst: 0.0253881
    Average: 0.00527926
    Total: 66.4448

Data_Preparation: 
    Best: 1.3e-06
    Worst: 0.0002168
    Average: 0.000423875
    Total: 5.33489

SAT: 
    Best: 1e-06
    Worst: 0.025084
    Average: 0.00484826
    Total: 61.0202

