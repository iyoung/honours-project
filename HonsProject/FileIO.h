#pragma once
#include <vector>
#include <string>
#include <glm\glm.hpp>
namespace FILEIO
{
    class FileIO
    {
    public:
        static char* loadFile(const char *pFileName, int &pFileSize);
        static void saveFile(const char* pDestFile, const char* pData);
		static void loadFile( const std::string& pFileName , std::vector<std::string>& );
        //TODO: complete this method
        static void constructFilePath(char* pDestPath, char* pSourcePath){;}
		static bool loadOBJ( char* pPath, std::vector<glm::vec3>& out_vertices, std::vector<glm::vec3>& out_normals, std::vector<glm::vec2>& out_uvs );
    };
};
