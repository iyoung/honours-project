// phong.vert
// Vertex shader for use with a Phong or other reflection model fragment shader
// Calculates and passes on V, L, N vectors for use in fragment shader, phong2.frag
#version 330 core

 uniform mat4 viewMatrix;
 uniform mat4 modelMatrix;
 uniform mat4 Projection;
 uniform vec4 LightPos;
 uniform vec4 eyePos;
 in vec3 in_Position;
 in vec3 in_Normal;

out vec3 ex_L;
out vec3 ex_V;
out vec3 ex_N;

// multiply each vertex position by the MVP matrix
// and find V, L, N vectors for the fragment shader
void main(void) {
	// vertex into eye coordinates
	vec4 vertexPosition = modelMatrix * vec4(in_Position,1.0);
	mat3 normalMatrix = mat3(transpose(inverse(modelMatrix)));
	vec3 vertexNormal = normalMatrix * in_Normal.xyz;
	ex_N = vertexNormal;
	//calculate view and light vectors in model space
	ex_V = eyePos.xyz-vertexPosition.xyz;
	ex_L = LightPos.xyz-vertexPosition.xyz;
    gl_Position = Projection * viewMatrix * vertexPosition;
}