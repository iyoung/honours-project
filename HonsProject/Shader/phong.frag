// Phong fragment shader phong.frag
// matched with phong.vert
#version 330 core

// Some drivers require the following
precision highp float;
struct LightData
{
    vec4 mDiffuse;
	vec4 mPosition;
	vec3 mAttenuation;
};
 uniform LightData light;
in vec3 ex_L;
in vec3 ex_V;
in vec3 ex_N;
uniform vec4 colour;
out vec4 out_Color;
 
void main(void) {   
	float intensity = 150.0;
	float lightDistance = length(ex_L);
	float kl, kq, kc;
	kc = light.mAttenuation.r;
	kl = light.mAttenuation.g*lightDistance;
	kq = light.mAttenuation.b*lightDistance*lightDistance;
	float att = 1.0 / (kc+kl+kq);
	vec3 amb = colour.rgb*light.mDiffuse.rgb*0.1;
	vec3 R = normalize(reflect(normalize(ex_L),normalize(ex_N)));
	vec3 diff = max(0.0,dot(normalize(ex_L),normalize(ex_N))) * light.mDiffuse.rgb*intensity*colour.rgb;
	vec3 spec = pow(max(0.0,dot(normalize(ex_V),R)),75) * light.mDiffuse.rgb*intensity*colour.rgb;
	out_Color = vec4(amb+att*(diff+spec),1.0);
}