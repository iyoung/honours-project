#include "PhysCollisionTask.h"
#include <glm\glm.hpp>
#include <math.h>
#include "Body.h"
#include "Material.h"
#include "Entity.h"
#include <algorithm>
PhysCollisionTask::PhysCollisionTask()
{
	status = false;
}
PhysCollisionTask::PhysCollisionTask(CollisionData* pData)
{
	data = pData;
}
void PhysCollisionTask::setData(void * pData)
{
	data = (CollisionData*)pData;
}

void PhysCollisionTask::execute()
{
	glm::vec3 normal = data->mNormal;
	Body* A = data->A;
	Body* B = data->B;
	//calculate relative velocity of the two bodies and the amount of this along the collision normal
	glm::vec3 relativeVel = B->getVelocity() - A->getVelocity();
	float velDotNorm = glm::dot(relativeVel, normal);
	//if the value is positive, then the objects are moving away from one another, so don't calculate anything.
	if (velDotNorm >= 0.0f)
		return;
	//calculate coefficient of restitution
	float rest = std::min(A->getMaterial()->getRestitution(), B->getMaterial()->getRestitution());
	//calculate the normal force magnitude scalar
	float j = -(1.0f + rest) * velDotNorm;

	glm::vec3 impulse;
	float combinedInverseMass;
	if (A->getBodyType() == BODY_STATIC)
	{
		combinedInverseMass = B->getInverseMass();
	}
	else if (B->getBodyType() == BODY_STATIC)
	{
		combinedInverseMass = A->getInverseMass();
	}
	else
	{
		combinedInverseMass = A->getInverseMass() + B->getInverseMass();
	}
	j /= combinedInverseMass;

	impulse = j * normal;
	glm::vec3 tD(0.0f);
	//Compute tangential relative motion for friction calculations
	glm::vec3 tV = relativeVel - normal*velDotNorm;
	if (glm::length(tV) > 0)
	{
		tD = glm::normalize(tV);
	}
	//calculate the direction of the frictional force

	//now we have the direction of the frictional force, we need to calculate the magnitude.
	//Ff = Fk*FN
	float uA = A->getMaterial()->getKineticFriction();
	float uB = B->getMaterial()->getKineticFriction();
	float Jt = glm::max(uA, uB)*velDotNorm / combinedInverseMass;
	glm::vec3 Ff = tD*Jt;
	glm::vec3 netforce = (Ff)+(impulse);
	if (glm::dot(netforce, netforce) > 0.5f)
	{
		A->wakeup();
		B->wakeup();
	}
	//Apply collision impulses
	SDL_LockMutex(A->getMutex());
	A->setLinearImpulse(-netforce);
	SDL_UnlockMutex(A->getMutex());
	SDL_LockMutex(B->getMutex());
	B->setLinearImpulse(netforce);
	SDL_UnlockMutex(B->getMutex());
	//calculate angular effects here

	glm::vec3 c2a = data->mContactPointA - A->getOwner()->getPosition();
	glm::vec3 c2aperpPoint = glm::dot(A->getOwner()->getPosition(), netforce)*glm::normalize(netforce);
	glm::vec3 c2aperp = data->mContactPointA - A->getOwner()->getPosition();
	glm::vec3 torqueA = glm::cross((-Ff) + (-impulse), c2aperp);
	glm::vec3 c2b = data->mContactPointB - B->getOwner()->getPosition();

	glm::vec3 c2bperpPoint = glm::dot(B->getOwner()->getPosition(), netforce)*glm::normalize(netforce);
	glm::vec3 c2bperp = data->mContactPointA - B->getOwner()->getPosition();
	glm::vec3 torqueB = glm::cross((Ff)+(impulse), c2bperp);
	SDL_LockMutex(A->getMutex());
	A->setAngularImpulse(torqueA);
	SDL_UnlockMutex(A->getMutex());
	SDL_LockMutex(B->getMutex());
	B->setAngularImpulse(torqueB);
	SDL_UnlockMutex(B->getMutex());
	//this code accounts for floating point errors which accumulate due to Floating point error IEEE754
	float slop = 0.01f;
	glm::vec3 correction = glm::max(data->mPenetrationDepth - slop, 0.0f) / combinedInverseMass * 0.3f * normal;
	if (A->getBodyType() == BODY_DYNAMIC)
	{
		A->getOwner()->setPosition(A->getOwner()->getPosition() - correction*A->getInverseMass());
	}
	if (B->getBodyType() == BODY_DYNAMIC)
	{
		B->getOwner()->setPosition(B->getOwner()->getPosition() + correction*B->getInverseMass());
	}
	status = true;
}

void * PhysCollisionTask::getResults()
{
	return nullptr;
}

bool PhysCollisionTask::getCompleteStatus()
{
	return status;
}


PhysCollisionTask::~PhysCollisionTask()
{
}
