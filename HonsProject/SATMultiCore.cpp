#include "SATMultiCore.h"
#include "Application.h"
#include "SATTask.h"
#include "Shape.h"
#include "SDLTaskManager.h"
#include "CollisionTypes.h"
#include <SDL_thread.h>
#include "Profiler.h"

#define LOCAL_X 0
#define LOCAL_Y 1
#define LOCAL_Z 2
SATMultiCore::SATMultiCore()
{
}

void SATMultiCore::update(Application & pApp)
{
	PROFILER->startProfileSubSection( "Multi_Thread_NarrowPhase" , "Data_Preparation" );
	int i = 0;
	SDLTaskManager* pool = SDLTaskManager::TaskManagerInstance();
	SDL_mutex* mut = pool->mLock;
	SDL_LockMutex(mut);
	pool->taskCounter = 0;
	for (; i < mPairManager->getPairCount(); i++)
	{
		//SATTask* t = new SATTask();
		pool->addTask(new SATTask(mPairManager.get(), &pApp));
	}

	pool->start();
	SDL_UnlockMutex(mut);

	PROFILER->endProfileSubSection( "Multi_Thread_NarrowPhase" , "Data_Preparation" );
	PROFILER->startProfileSubSection( "Multi_Thread_NarrowPhase" , "SAT" );
	int tasks = pool->taskCounter;
	while (tasks > 0)
	{
		Task* task = nullptr;
		SDL_LockMutex(mut);
		unsigned int size = pool->mCurrentTasks.size();
		if (size > 0)
		{
			task = pool->mCurrentTasks.front();
			pool->mCurrentTasks.pop_front();
		}
		else
		{
			tasks = pool->taskCounter;
			pool->mRunning = false;
			SDL_UnlockMutex(mut);
			continue;

		}
		if (task != nullptr)
		{
			SDL_UnlockMutex(mut);
			task->execute();
		}
		else
		{
			pool->mRunning = false;
			tasks = pool->taskCounter;
			SDL_UnlockMutex(mut);
			continue;
		}

		SDL_LockMutex(mut);
		pool->taskCounter--;
		pool->addCompleteTask(task);
		tasks = pool->taskCounter;
		SDL_UnlockMutex(mut);
		task = nullptr;
	}
	SDL_LockMutex(mut);
	while (!pool->mCompleteTasks.empty())
	{
		auto task = pool->mCompleteTasks.front();
		pool->mCompleteTasks.pop_front();
		delete task;
	}
	SDL_UnlockMutex(mut);
	mPairManager->updatepairs();
	PROFILER->endProfileSubSection( "Multi_Thread_NarrowPhase" , "SAT" );
}

void SATMultiCore::computeNearestEdge(glm::vec3* data, int size, glm::vec3* line, glm::vec3 circleCentre)
{
	line[0] = data[0];
	line[1] = data[1];
	float distance0 = glm::length(circleCentre - data[0]);
	float distance1 = glm::length(circleCentre - data[1]);
	if (distance1 < distance0)
	{
		line[0] = line[1];
		line[1] = data[0];
		float d = distance0;
		distance0 = distance1;
		distance1 = d;
	}
	for (int i = 2; i < size; i++)
	{
		float l = glm::length(circleCentre - data[i]);
		if (l < distance0)
		{
			line[1] = line[0];
			distance1 = distance0;
			line[0] = data[i];
			distance0 = l;
		}
		else if (l < distance1)
		{
			line[1] = data[i];
			distance1 = l;
		}
	}
}

glm::vec3 SATMultiCore::SphereVSplaneContact(glm::vec3 pNormal, glm::vec3 sphereCentre, float radius, float planeDepth)
{

	return sphereCentre - (pNormal * (radius - planeDepth));
}

int SATMultiCore::computeNearestPointToSphere(glm::vec3 data[], int size, glm::vec3 pCircleCentre, float pRadius)
{
	glm::vec3 v = data[0] - pCircleCentre;
	int currentIndex=0;
	float distance = glm::dot(v, v);
	for (int i = 1; i < size; i++)
	{
		v = data[i] - pCircleCentre;
		float d2 = glm::dot(v, v);
		if (d2 < distance)
		{
			distance = d2;
			currentIndex = i;
		}
	}
	return currentIndex;
}

int SATMultiCore::computeFarthestPoint( glm::vec3 points[], int numPoints , glm::vec3 vector )
{
	float proj = glm::dot( points[ 0 ] , vector );
	int current = 0;
	for ( int i = 1; i < numPoints; i++ )
	{
		if ( glm::dot( points[ i ] , vector ) > proj )
		{
			proj = glm::dot( points[ i ] , vector );
			current = i;
		}
	}
	return current;
}

glm::vec3 SATMultiCore::computeContactPointSphereLine( glm::vec3 p_PointA , glm::vec3 p_PointB , glm::vec3 p_CircleCentre , float p_CircleRadius )
{
	//find the line
	//find the closest point on the line
	//find the distance between the centre of the circle and the line
	//if less than radius, collision is true
	//penetration depth = radius - (distance between circle centre and closest point)
	glm::vec3 v_Edge = p_PointB - p_PointA;
	glm::vec3 line = glm::normalize( v_Edge );
	glm::vec3 nearest = nearestPointCircleLine( p_PointA , p_PointB , p_CircleCentre );
	glm::vec3 normal;
	float start = glm::dot( p_PointA , line );
	float end = glm::dot( p_PointB , line );
	float nearestproj = glm::dot( nearest , line );
	return p_PointA + line*nearestproj;

}

float SATMultiCore::PPDistanceSquared(glm::vec3 & pStart, glm::vec3 & pEnd)
{
	glm::vec3 vector = pEnd-pStart;
	return glm::dot(vector,vector);
}

bool SATMultiCore::sphereVsLine(glm::vec3 p_PointA, glm::vec3 p_PointB, glm::vec3 p_CircleCentre, float p_CircleRadius, glm::vec3& pNormal)
{
	//find the line
	//find the closest point on the line
	//find the distance between the centre of the circle and the line
	//if less than radius, collision is true
	//penetration depth = radius - (distance between circle centre and closest point)
	glm::vec3 v_Edge = p_PointB - p_PointA;
	glm::vec3 line = glm::normalize( v_Edge );
	glm::vec3 nearest = nearestPointCircleLine( p_PointA , p_PointB , p_CircleCentre );
	float start = glm::dot( p_PointA , line );
	float end = glm::dot( p_PointB , line );
	float nearestproj = glm::dot( nearest , line );
	if ( start < nearestproj && nearestproj < end )
	{
		if ( glm::length( p_CircleCentre - nearest ) < p_CircleRadius )
		{
			//calculate normal, as collision detected
			pNormal =  p_CircleCentre - nearest ;
			return true;
		}
	}
	return false;
}

glm::vec3 SATMultiCore::nearestPointCircleLine( glm::vec3 pStart , glm::vec3 pEnd , glm::vec3 pCircleCentre )
{
	glm::vec3 line = glm::normalize( pEnd - pStart );
	float start = glm::dot( pStart , line );
	float end = glm::dot( pEnd , line );
	float circleProj = glm::dot( pCircleCentre , line );

	return pStart + line*( circleProj - start );

}

int SATMultiCore::computeMinProjection(glm::vec3 data[], int size, glm::vec3 axis)
{
	int index = 0;
	float projection = glm::dot(data[0], axis);
	for (int i = 1; i < size; i++)
	{
		float proj = glm::dot(data[i], axis);
		if (proj < projection)
		{
			index = i;
			projection = proj;
		}
	}
	return index;
}

int SATMultiCore::computeMaxProjection(glm::vec3 data[], int size, glm::vec3 axis)
{
	int index = 0;
	float projection = glm::dot(data[0], axis);
	for (int i = 1; i < size; i++)
	{
		float proj = glm::dot(data[i], axis);
		if (proj > projection)
		{
			index = i;
			projection = proj;
		}
	}
	return index;
}

float SATMultiCore::computeMinProj(glm::vec3 data[], int size, glm::vec3 axis)
{
	
	float projection = glm::dot(data[0], axis);
	for (int i = 1; i < size; i++)
	{
		float proj = glm::dot(data[i], axis);
		if (proj < projection)
		{
			projection = proj;
		}
	}
	return projection;
}

float SATMultiCore::computeMaxProj(glm::vec3 data[], int size, glm::vec3 axis)
{
	float projection = glm::dot(data[0], axis);
	for (int i = 1; i < size; i++)
	{
		float proj = glm::dot(data[i], axis);
		if (proj > projection)
		{
			projection = proj;
		}
	}
	return projection;
}

bool SATMultiCore::planeSphereIntersects(glm::vec3 & normal, float & planePosition, float & radius, glm::vec3 & sphereCentre, float & penetration, glm::vec3& contact)
{
	float sphereProj = glm::dot(sphereCentre, normal);
	penetration = sphereProj - planePosition;
	if (penetration > 0.0f && penetration < radius)
	{
		contact = sphereCentre - normal * (radius - penetration);
		return true;
	}
	penetration = radius - penetration;
	return false;
}

bool SATMultiCore::lineSphereIntersects(glm::vec3 & line, glm::vec2 & extents, float & radius, glm::vec3 & sphereCentre, float penetration, glm::vec3& contact)
{
	float sphereProj = glm::dot(sphereCentre, line);
	glm::vec3 nearest = line*sphereProj;
	penetration = glm::length(nearest - sphereCentre);
	if (penetration < radius)
	{
		contact = nearest;
		return true;
	}
	return false;
}

bool SATMultiCore::pointSphereIntersects(glm::vec3 & point, float & radius, glm::vec3 & sphereCentre, float& penetration)
{
	glm::vec3 diff = sphereCentre - point;
	float absdiff = glm::dot(diff, diff);
	float radsquared = radius*radius;
	if (absdiff < radius*radius)
	{
		penetration = sqrt(radsquared - absdiff);
		return true;
	}
	return false;
}

void SATMultiCore::sphereVSplane(Pair* pPair, CollisionEvent& e)
{
	//we are going to project the sphere only the plane and compare height of the centre of the circle
	// +- radius to the plane. 
	//When we project it we will be able to tell if the circle is intersecting
	Sphere* sphere = nullptr;
	Plane* plane = nullptr;
	glm::vec3 sphereP;
	//First, get the body which has a sphere shape, and work from there
	CollisionData c;
	//this collision check is from the plane's frame of reference, so A will always be the plane.
	if(pPair->A->getShape()->getShapeType()==SHAPE_SPHERE)
	{
		 sphere = (Sphere*)pPair->A->getShape();
		 plane = (Plane*)pPair->B->getShape();
		 c.A = pPair->B;
		 c.B = pPair->A;
	}
	else
	{
		sphere = (Sphere*)pPair->B->getShape();
		plane = (Plane*)pPair->A->getShape();
		c.A = pPair->A;
		c.B = pPair->B;
	}

	//data from sphere
	sphereP = sphere->getOwner()->getOwner()->getPosition();
	float radius = sphere->getRadius();

	//data from plane
	glm::vec3 planeP = plane->getOwner()->getOwner()->getPosition();
	glm::vec3 planeAxes[2];
	glm::vec3 planePoints[4];
	glm::vec3 planeNormal;
	
	//get the rotation matrix from the plane
	glm::mat3 transform = glm::mat3(glm::toMat4(plane->getOwner()->getOwner()->getOrientation()));


	float HalfWidth = plane->getWidth()*0.5f;
	float halfLength = plane->getLength()*0.5f;
	//now calculate the points of the plane and its axes for SAT checking.
	//calculate points
	planePoints[ 0 ] = planeP + transform * glm::vec3( - HalfWidth , 0.0f,  halfLength ); 
	//back left
	planePoints[ 1 ] = planeP + transform * glm::vec3(  HalfWidth , 0.0f ,  halfLength );
	//back right
	planePoints[ 2 ] = planeP + transform * glm::vec3(  HalfWidth , 0.0f , -halfLength ); 
	//front right
	planePoints[ 3 ] = planeP + transform * glm::vec3(  HalfWidth , 0.0f , -halfLength ); 
	//front left
	
	 //calculate axes
	planeAxes[0] = glm::normalize(planePoints[1]-planePoints[0]);
	planeAxes[1] = glm::normalize(planePoints[2]-planePoints[1]);
	//amnd normal vector
	planeNormal = glm::cross(planeAxes[0],planeAxes[1]);

	//now we have all the data we need to work out intersection so proceed.

	float normalProj, Xproj, Zproj, planeProj;

	normalProj = glm::dot(sphereP, planeNormal);
	Xproj = glm::dot(sphereP, planeAxes[0]);
	Zproj = glm::dot(sphereP, planeAxes[1]);
	planeProj = glm::dot(planeP, planeNormal);

	//if the distance between the sphere and the plane, on the surface normal is less than the radius of the sphere
	//then we check the axes
	float diff = normalProj - planeProj;
	float abs = glm::abs(diff);
	if (abs > radius)
	{
		//check for previous contact
		if (pPair->mColliding)
		{

			e.setCollisionData(EVENT_COLLISION_END, c);
			pPair->mColliding = false;
		}
		else
		{
			e.setCollisionData(EVENT_NO_COLLISION, c);
			pPair->mColliding = false;
		}
		return;
	}
	else
	{
		//0 is for minimum, 1 is for maximum
		glm::vec2 planeMinMax[2];
		planeMinMax[0].x = glm::dot(planePoints[0], planeAxes[0]);
		planeMinMax[0].y = glm::dot(planePoints[1], planeAxes[1]);
		planeMinMax[1].x = glm::dot(planePoints[1], planeAxes[0]);
		planeMinMax[1].y = glm::dot(planePoints[3], planeAxes[1]);

		glm::vec2 sphereMinMax[2];
		sphereMinMax[0].x = glm::dot(sphereP, planeAxes[0]) - radius;
		sphereMinMax[0].y = glm::dot(sphereP, planeAxes[1]) - radius;
		sphereMinMax[1].x = glm::dot(sphereP, planeAxes[0]) + radius;
		sphereMinMax[1].y = glm::dot(sphereP, planeAxes[1]) + radius;
		
		//now we check to see if the sphere is close enough within the area of the plane to be
		//contained on one axis (position is such that surface intersection is possible)
		if (sphereMinMax[1].x < planeMinMax[0].x || sphereMinMax[0].x > planeMinMax[1].x)
		{
			//check for previous contact
			if (pPair->mColliding)
			{
				c.A = pPair->A;
				c.B = pPair->B;
				e.setCollisionData(EVENT_COLLISION_END, c);
				pPair->mColliding = false;
			}
			else
			{
				e.setCollisionData(EVENT_NO_COLLISION, c);
				pPair->mColliding = false;
			}
			return;
		}
		else
		{
		//containment on second axis
			if(sphereMinMax[1].y < planeMinMax[0].y || sphereMinMax[0].y > planeMinMax[1].y)
			{
				//check for previous contact
				if (pPair->mColliding)
				{
					c.A = pPair->A;
					c.B = pPair->B;
					e.setCollisionData(EVENT_COLLISION_END, c);
					pPair->mColliding = false;
				}
				{
					e.setCollisionData(EVENT_NO_COLLISION, c);
					pPair->mColliding = false;
				}
				return;
			}
			else
				//if we reach here, then we have an intersection
			{
				c.mNormal = planeNormal;

				//c.mContactPointB = 
				//sphere is above the plane
		
				c.mPenetrationDepth = radius - glm::abs(normalProj - planeProj);


				c.mContactPointA = sphereP + c.mNormal * radius - c.mPenetrationDepth;
				c.mContactPointB = c.mContactPointA;

				//now we decide which type of event to send
				if (pPair->mColliding)
				{

					e.setCollisionData(EVENT_COLLISION, c);
				}
				else
				{
					e.setCollisionData(EVENT_COLLISION_BEGIN, c);
					pPair->mColliding = true;
				}
			}
		}
	}
}

void SATMultiCore::sphereVScuboid(Pair* pPair, CollisionEvent& e)
{

	//Cases:
	//1. Sphere-face intersection: sphere position contained within bounds of OBB on two axes and overlap on other axis
	//2. Sphere-edge intersection: sphere contained on one axis and overlaps on other two axes
	//3. Sphere-point intersection: no axis containment, but overlap on three axes.
	//4. no intersection: no overlap or containment on at least one axis
	Sphere* sphere = nullptr;
	Cuboid* cube = nullptr;
	//first, construct data
	//this is calculated from the cubes frame of reference, so A will always be the cube
	CollisionData c;

	if (pPair->A->getShape()->getShapeType() == SHAPE_SPHERE)
	{
		sphere = (Sphere*)pPair->A->getShape();
		cube = (Cuboid*)pPair->B->getShape();
		c.A = pPair->B;
		c.B = pPair->A;
	}
	else
	{
		sphere = (Sphere*)pPair->B->getShape();
		cube = (Cuboid*)pPair->A->getShape();
		c.A = pPair->A;
		c.B = pPair->B;
	}

	glm::mat3 cubeTransform = glm::mat3(glm::toMat4(cube->getOwner()->getOwner()->getOrientation()));
	glm::vec3 cubePos = cube->getOwner()->getOwner()->getPosition();
	glm::vec3 sphereCentre = sphere->getOwner()->getOwner()->getPosition();
	float radius = sphere->getRadius();
	float HalfWidth, halfLength, halfHeight;

	HalfWidth = cube->getWidth()*0.5f;
	halfHeight = cube->getHeight()*0.5f;
	halfLength = cube->getLength()*0.5f;

	glm::vec3 cubePoints[8];
	cubePoints[0] = cubePos + (cubeTransform*glm::vec3(-HalfWidth, halfHeight, -halfLength)); //back top left
	cubePoints[1] = cubePos + (cubeTransform*glm::vec3(HalfWidth, halfHeight, -halfLength)); //back top right
	cubePoints[2] = cubePos + (cubeTransform*glm::vec3(HalfWidth, halfHeight, halfLength)); //front top right
	cubePoints[3] = cubePos + (cubeTransform*glm::vec3(-HalfWidth, halfHeight, halfLength)); //front top left
	cubePoints[4] = cubePos + (cubeTransform*glm::vec3(-HalfWidth, -halfHeight, -halfLength)); //back bottom left
	cubePoints[5] = cubePos + (cubeTransform*glm::vec3(HalfWidth, -halfHeight, -halfLength)); //back bottom right
	cubePoints[6] = cubePos + (cubeTransform*glm::vec3(HalfWidth, -halfHeight, halfLength)); //front bottom right
	cubePoints[7] = cubePos + (cubeTransform*glm::vec3(-HalfWidth, -halfHeight, halfLength)); //front bottom left
	glm::vec3 axes[3];
	axes[LOCAL_X] = glm::normalize(cubePoints[1] - cubePoints[0]); //local x axis
	axes[LOCAL_Y] = glm::normalize(cubePoints[0] - cubePoints[4]); //local y axis
	axes[LOCAL_Z] = glm::normalize(cubePoints[2] - cubePoints[1]); //local z axis
	glm::vec2 minMax[3];


	float penetration = 0.0f;
	glm::vec3 normal;
	bool penetrationset = false;
	//sphere/face check first. This is a mid phase check
	//for each plane normal:
	//find min and max extents for cube
	//find position of sphere on normals
	float sphereProjections[3];
	for (size_t i = 0; i < 3; i++)
	{
		minMax[i].x = glm::dot(cubePoints[computeMinProjection(cubePoints, 8, axes[i])], axes[i]); //this is one plane
		minMax[i].y = glm::dot(cubePoints[computeMaxProjection(cubePoints, 8, axes[i])], axes[i]); //this is another
		sphereProjections[i] = glm::dot(sphereCentre, axes[i]);
	}
	int facesphereCount = 0;

	for (size_t i = 0; i < 3; i++)
	{
		float sphereProjminusrad = sphereProjections[i] - radius;
		float sphereProjplusrad = sphereProjections[i] + radius;
		float cubeProjMin = minMax[i].x;
		float cubeProjMax = minMax[i].y;
		float sphereProj = sphereProjections[i];
		float cubeProjC = glm::dot(cubePos, axes[i]);
		//look for axis of separation ie no collision
		//                cubeMin     cubeMax
		//                 |------------|
		// |------------|
		// SphereMin  SphereMax
		//or 
		// cubeMin   cubeMax
		// |------------|
		//                 |------------|
		//              sphereMin     sphereMax
		if (sphereProjminusrad > cubeProjMax || sphereProjplusrad < cubeProjMin)
		{
			//no collision possible: An axis of separation exists
			if (pPair->mColliding)
			{

				e.setCollisionData(EVENT_COLLISION_END, c);
				pPair->mColliding = false;
				return;
			}
			else
			{
				e.setCollisionData(EVENT_NO_COLLISION, c);
				pPair->mColliding = false;
				return;
			}
		}
		//check for containment of sphere position within cube projections on axis
		//			  cubeMin           cubeMax
		//				|------------------|
		//					|<--------|-------->|
		//				   s-r  sphereCentre   s+r
		if (sphereProj > cubeProjMin && sphereProj < cubeProjMax)
		{
			//containment on one axis so a a sphere/edge or sphere/face collision is likely, if all axes are positive in some way.

			//record data:
			//increment face/intersection count.
			//find which of the faces has the smallest intersection, and record it, if it is less than the current.
			//if overlap towards min is less than overlap towards right
			//record -axis, else record axis
			facesphereCount++;
			glm::vec3 axis;
			float current;
			//      SphereProj
			//         |
			//------------------------------------------
			//               |
			//             cubeProjC
			//<--------Normal Push sphere this way
			if (sphereProj < cubeProjC)
			{
				current =  sphereProjplusrad - cubeProjMin;
				axis = -axes[i];
			}
			//                 SphereProj
			//						|
			//------------------------------------------
			//                 |
			//             cubeProjC
			//              Normal------>Push sphere this way
			else
			{
				current = cubeProjMax - sphereProjminusrad;
				axis = axes[i];
			}
			if ( penetrationset == false || glm::abs(current) < penetration)
			{
				penetration = current;
				penetrationset = true;
				normal = axis;
			}

		}
		//by process of elimination, we have an overlap intersection on current axis
		//						cubeMin           cubeMax
		//							|----------------|
		//					|------------|
		//				sphereMin    sphereMax
		//or
		//      cubeMin             CubeMax
		//		  |--------------------|
		//                        |---------------|
		//					sphereMin           SphereMax
		else
		{
			
			//find minimum overlap on axis and record if less than current.
			//if x, then -axis, else axis
			float p = 0.0f;			
			glm::vec3 axis;
			if (cubeProjC > sphereProj)
			{
				p = sphereProjplusrad - cubeProjMin;
				axis = -axes[i];
			}
			else
			{
				p = cubeProjMax - sphereProjminusrad;
				axis = axes[i];
			}
			if (!penetrationset || glm::abs(p) < penetration)
			{
				penetration = p;
				normal = axis;
				penetrationset = true;
			}
		}
	}

	//by reaching this point, our test is positive.
	//check face/sphere intersect count, to discern how to compute contact point.
	//if face sphere intersection count is 3, sphere is contained within the cube: Edge case, but not impossible, so we handle it.
	//if face/sphere intersection count is 2 then we have a sphere/face intersection
	//if face/sphere intersection count is 1, then we have a sphere/edge intersection
	//if face/sphere intersection count is 0, then we have a possible false positive so we check for point/sphere intersection
	c.mNormal = normal;
	c.mPenetrationDepth = penetration;
	switch (facesphereCount)
	{
		case SpherePoint:
		{
			//compute nearest point to sphere, and use it as contact point B & A
			glm::vec3 p = cubePoints[computeNearestPointToSphere(cubePoints, 8, sphereCentre, radius)];
			glm::vec3 linetoSphereCentre = glm::normalize(sphereCentre - p);
			float pointonline = glm::dot(linetoSphereCentre, p);
			float sphereonline = glm::dot(sphereCentre, p);
			if ((sphereonline-pointonline) < radius)
			{
				//positive collision
				//recalculate normal and penetration
				c.mNormal = linetoSphereCentre;
				c.mPenetrationDepth = radius - glm::length(linetoSphereCentre);
				c.mContactPointA = p;
				c.mContactPointB = p;
			}
			else
			{
				//false positive
				if (pPair->mColliding)
				{
					e.setCollisionData(EVENT_COLLISION_END, c);
					pPair->mColliding = false;

				}
				else
				{
					e.setCollisionData(EVENT_NO_COLLISION, c);
					pPair->mColliding = false;
				}
				return;
			}
		}
		break;
		case SphereEdge:
		{
			//compute nearest edge and find closest point in edge. use this as contact points
			glm::vec3 line[2]; //nearest two points, representing an edge
			computeNearestEdge(cubePoints, 8, line, sphereCentre);
			glm::vec3 p = computeContactPointSphereLine(line[0], line[1], sphereCentre, radius);
			c.mContactPointA = cubePos + p;
			c.mContactPointB = cubePos + p;
			c.mNormal = glm::normalize(sphereCentre - p);
			c.mPenetrationDepth = glm::abs(glm::length(sphereCentre - p) - radius);
		}
		break;
		default:
		{
			//compute contact point on face by using sphere centre - normal * (radius-penetration);
			c.mContactPointA = SphereVSplaneContact(c.mNormal, sphereCentre, radius, c.mPenetrationDepth);
			c.mContactPointB = c.mContactPointA;
		}
		break;
		}
		//if we reach this point, false positives are not possible, so send event
		if (pPair->mColliding)
		{
			e.setCollisionData(EVENT_COLLISION, c);
		}
		else
		{
			e.setCollisionData(EVENT_COLLISION_BEGIN, c);
			pPair->mColliding = true;
		}
}

void SATMultiCore::cuboidVSplane(Pair* pPair, CollisionEvent& e)
{

	Cuboid* cube = nullptr;
	Plane* plane = nullptr;
	//uninitialised cuboid data
	glm::mat3 cubeTransform;
	glm::vec3 cubeAxes[3];
	glm::vec3 cubePoints[8];
	glm::vec3 cubePos;
	//unititialised plane 
	glm::mat3 planeTransform;
	glm::vec3 planePoints[4];
	glm::vec3 planeAxes[2];
	glm::vec3 planeNormal;
	glm::vec3 planePos;
	CollisionData c;


	//do some basic casting to figure out which shape is which
	//we calculate this from the plane's frame of reference, so the plane will always be A
	if (pPair->A->getShape()->getShapeType() == SHAPE_CUBOID)
	{
		cube = (Cuboid*)pPair->A->getShape();
		plane = (Plane*)pPair->B->getShape();
		cubeTransform = glm::mat3(glm::toMat4(pPair->A->getOwner()->getOrientation()));
		planeTransform = glm::mat3(glm::toMat4(pPair->B->getOwner()->getOrientation()));
		c.A = pPair->B;
		c.B = pPair->A;
	}
	else
	{
		cube = (Cuboid*)pPair->B->getShape();
		plane = (Plane*)pPair->A->getShape();
		cubeTransform = glm::mat3(glm::toMat4(pPair->B->getOwner()->getOrientation()));
		planeTransform = glm::mat3(glm::toMat4(pPair->A->getOwner()->getOrientation()));
		c.A = pPair->A;
		c.B = pPair->B;
	}

	cubePos = cube->getOwner()->getOwner()->getPosition();
	planePos = plane->getOwner()->getOwner()->getPosition();

	float HalfWidth = cube->getWidth()*0.5f;
	float halfHeight = cube->getHeight()*0.5f;
	float halfLength = cube->getLength()*0.5f;
		//generate data for cuboid
	cubePoints[ 0 ] = cubePos + ( cubeTransform*glm::vec3( -HalfWidth , halfHeight , halfLength ) ); //back top left
	cubePoints[ 1 ] = cubePos + ( cubeTransform*glm::vec3( HalfWidth , halfHeight , halfLength ) ); //back top right
	cubePoints[ 2 ] = cubePos + ( cubeTransform*glm::vec3( HalfWidth , halfHeight , -halfLength ) ); //front top right
	cubePoints[ 3 ] = cubePos + ( cubeTransform*glm::vec3( -HalfWidth , halfHeight , -halfLength ) ); //front top left
	cubePoints[ 4 ] = cubePos + ( cubeTransform*glm::vec3( -HalfWidth , -halfHeight , halfLength ) ); //back bottom left
	cubePoints[ 5 ] = cubePos + ( cubeTransform*glm::vec3( HalfWidth , -halfHeight , halfLength ) ); //back bottom right
	cubePoints[ 6 ] = cubePos + ( cubeTransform*glm::vec3( HalfWidth , -halfHeight , -halfLength ) ); //front bottom right
	cubePoints[ 7 ] = cubePos + ( cubeTransform*glm::vec3( -HalfWidth , -halfHeight , -halfLength ) ); //front bottom left

	cubeAxes[0] = glm::normalize(cubePoints[1] - cubePoints[0]);
	cubeAxes[1] = glm::normalize(cubePoints[2] - cubePoints[1]);
	cubeAxes[2] = glm::normalize(cubePoints[4] - cubePoints[0]);

	HalfWidth = plane->getWidth()*0.5f;
	halfLength = plane->getLength()*0.5f;
	//generate data for plane
	planePoints[ 0 ] = planePos + ( planeTransform*glm::vec3( -HalfWidth , 0.0f , halfLength ) ); //back left
	planePoints[ 1 ] = planePos + ( planeTransform*glm::vec3( HalfWidth , 0.0f , halfLength ) ); //back right
	planePoints[ 2 ] = planePos + ( planeTransform*glm::vec3( HalfWidth , 0.0f , -halfLength ) ); //front right
	planePoints[ 3 ] = planePos + ( planeTransform*glm::vec3( -HalfWidth , 0.0f , -halfLength ) ); //front left

	planeAxes[0] = glm::normalize(planePoints[1] - planePoints[0]);
	planeAxes[1] = glm::normalize(planePoints[1] - planePoints[2]);

	planeNormal = glm::cross(planeAxes[1], planeAxes[0]);

	//now we check for SAT intersections
	//first, check that the cube is within the bounds of the plane on it's local X,Z axes.
	//then, if no axis of separation, check the plane normal.
	glm::vec3 translationVector;
	float finalPenetrationDepth = 0.0f;
	float cubeMin, cubeMax, planeMin, planeMax;
	//algorithm
	cubeMin = computeMinProj(cubePoints, 8, planeAxes[0]);
	cubeMax = computeMaxProj(cubePoints, 8, planeAxes[0]);
	planeMin = computeMinProj(planePoints, 4, planeAxes[0]);
	planeMax = computeMaxProj(planePoints, 4, planeAxes[0]);
	//check for seperation on current axis
	if (cubeMin > planeMax || planeMin > cubeMax)
	{
		//separation found
		//check for previous contact
		if (pPair->mColliding)
		{

			e.setCollisionData(EVENT_COLLISION_END, c);
			pPair->mColliding = false;
		}
		else
		{
			e.setCollisionData(EVENT_NO_COLLISION, c);
			pPair->mColliding = false;
		}
		//then discard pair
	}
	cubeMin = computeMinProj(cubePoints, 8, planeAxes[1]);
	cubeMax = computeMaxProj(cubePoints, 8, planeAxes[1]);
	planeMin = computeMinProj(planePoints, 4, planeAxes[1]);
	planeMax = computeMaxProj(planePoints, 4, planeAxes[1]);
	//check for seperation on current axis
	if (cubeMin > planeMax || planeMin > cubeMax)
	{
		//separation found
		//check for previous contact
		if (pPair->mColliding)
		{
			e.setCollisionData(EVENT_COLLISION_END, c);
			pPair->mColliding = false;
		}
		else
		{
			e.setCollisionData(EVENT_NO_COLLISION, c);
			pPair->mColliding = false;
		}
		return;
	}
	//if we reach this stage, then we check the cube points against the plane surface normal.
	cubeMin = computeMinProj(cubePoints, 8, planeNormal);
	cubeMax = computeMaxProj(cubePoints, 8, planeNormal);
	planeMin = computeMinProj(planePoints, 4, planeNormal);
	if(cubeMin > planeMin || cubeMax < planeMin)
	{
		//separation found
		//check for previous contact
		if (pPair->mColliding)
		{
			e.setCollisionData(EVENT_COLLISION_END, c);
			pPair->mColliding = false;
		}
		else
		{
			e.setCollisionData(EVENT_NO_COLLISION, c);
			pPair->mColliding = false;
		}
		return;
	}
	//if this stage is reached, then we have intersections or containments on all axes
	finalPenetrationDepth = (planeMin-cubeMin);
	//so now calculate the collision point and deal with the various collision scenarios(begin, continue)
	c.mNormal = planeNormal;
	c.mPenetrationDepth = glm::abs( finalPenetrationDepth);
	
	//calculate collision points by determimining the closest point to the centre of the centre of mass to the first shape.
	c.mContactPointA = cubePoints[computeMinProjection(cubePoints, 8, c.mNormal)] + c.mNormal*c.mPenetrationDepth;
	c.mContactPointB = c.mContactPointA;


	//and pass on contact manifold to app to handle resolution of contact.

	if (pPair->mColliding)
	{
		e.setCollisionData(EVENT_COLLISION, c);
		pPair->mColliding = true;
	}
	else
	{
		e.setCollisionData(EVENT_COLLISION_BEGIN, c);
		pPair->mColliding = true;
	}
}

void SATMultiCore::cuboidVScuboid(Pair* pPair, CollisionEvent& e)
{
	//actual implementation of SAT algorithm for OBB vs OBB:
	//get Body and shape details
	Cuboid* A = (Cuboid*)pPair->A->getShape();
	Cuboid* B = (Cuboid*)pPair->B->getShape();
	//get cube positions in world space
	glm::vec3 cubePA = A->getOwner()->getOwner()->getPosition();
	glm::vec3 cubePB = B->getOwner()->getOwner()->getPosition();
	//get extents for OBBs
	glm::vec3 eA(A->getWidth()*0.5f, A->getHeight()*0.5f, A->getLength()*0.5f);
	glm::vec3 eB(B->getWidth()*0.5f, B->getHeight()*0.5f, B->getLength()*0.5f);
	//get rotation transform
	//These matrices define the axes we will be using to perform our separating axes tests.
	glm::mat3 transformA = glm::mat3(glm::toMat4(A->getOwner()->getOwner()->getOrientation()));
	glm::mat3 transformB = glm::mat3(glm::toMat4(B->getOwner()->getOwner()->getOrientation()));

	//Now we need to generate the points that each cube has in world space. Not ideal, but I don't know how to do it in A's frame yet.
	glm::vec3 pointsA[8] = {
		cubePA + transformA*glm::vec3(-eA.x,-eA.y,-eA.z),
		cubePA + transformA*glm::vec3(eA.x,-eA.y,-eA.z),
		cubePA + transformA*glm::vec3(eA.x, eA.y,-eA.z),
		cubePA + transformA*glm::vec3(eA.x, eA.y, eA.z),
		cubePA + transformA*glm::vec3(-eA.x, eA.y,-eA.z),
		cubePA + transformA*glm::vec3(-eA.x, eA.y, eA.z),
		cubePA + transformA*glm::vec3(-eA.x,-eA.y, eA.z),
		cubePA + transformA*glm::vec3(eA.x,-eA.y, eA.z)
	};
	glm::vec3 pointsB[8] = {
		cubePB + transformB*glm::vec3(-eB.x,-eB.y,-eB.z),
		cubePB + transformB*glm::vec3(eB.x,-eB.y,-eB.z),
		cubePB + transformB*glm::vec3(eB.x, eB.y,-eB.z),
		cubePB + transformB*glm::vec3(eB.x, eB.y, eB.z),
		cubePB + transformB*glm::vec3(-eB.x, eB.y,-eB.z),
		cubePB + transformB*glm::vec3(-eB.x, eB.y, eB.z),
		cubePB + transformB*glm::vec3(-eB.x,-eB.y, eB.z),
		cubePB + transformB*glm::vec3(eB.x,-eB.y, eB.z)
	};
	//some basic data to work with
	float minA, minB, maxA, maxB, posA, posB;
	bool hasOverlap = false;
	glm::vec3 normal;
	float penetration = 0.0f;
	bool penetrationset = false;
	//basic collision data
	CollisionData c;
	c.A = pPair->A;
	c.B = pPair->B;
	//now the algorithm proper. 
	//For each axis in transforms
	//for each OBB, 
	//compute min and max projections.
	//compute projection of OBB centres: posA posB, this reduces the 3d problem to a 1d number line problem
	//first, based on A
	for (int i = 0; i < 3; i++)
	{
		minA = computeMinProj(pointsA, 8, transformA[i]);
		maxA = computeMaxProj(pointsA, 8, transformA[i]);
		minB = computeMinProj(pointsB, 8, transformA[i]);
		maxB = computeMaxProj(pointsB, 8, transformA[i]);
		posA = glm::dot(cubePA, transformA[i]);
		posB = glm::dot(cubePB, transformA[i]);
		float intersection = 0.0f;
		glm::vec3 axis;
		//if A is to the left of B
		if (posA < posB)
		{
			intersection = maxA - minB;
			axis = transformA[i];
		}
		//B is left of A
		else
		{
			intersection = maxB - minA;
			axis = -transformA[i];
		}
		if (intersection < 0.0f)
		{
			//axis of separation exists. No collision
			if (pPair->mColliding)
			{
				e.setCollisionData(EVENT_COLLISION_END, c);
				pPair->mColliding = false;
			}
			else
			{
				e.setCollisionData(EVENT_NO_COLLISION, c);
				pPair->mColliding = false;
			}
		}
		else
		{
			if (!penetrationset || intersection < penetration)
			{
				penetration = intersection;
				normal = axis;
				penetrationset = true;
			}
		}
	}
	//then based on B
	for (int i = 0; i < 3; i++)
	{
		minA = computeMinProj(pointsA, 8, transformB[i]);
		minA = computeMinProj(pointsA, 8, transformB[i]);
		minB = computeMaxProj(pointsB, 8, transformB[i]);
		maxB = computeMaxProj(pointsB, 8, transformB[i]);
		posA = glm::dot(cubePA, transformB[i]);
		posB = glm::dot(cubePB, transformB[i]);
		float intersection = 0.0f;
		glm::vec3 axis;
		//if A is to the left of B
		if (posA < posB)
		{
			intersection = (posA + maxA) - (posB - minB);
			axis = transformB[i];
		}
		//B is left of A
		else
		{
			intersection = (posB + maxB) - (posA - minA);
			axis = -transformB[i];
		}
		if (intersection < 0.0f)
		{
			//axis of separation exists. No collision
			if (pPair->mColliding)
			{
				e.setCollisionData(EVENT_COLLISION_END, c);
				pPair->mColliding = false;
			}
			else
			{
				e.setCollisionData(EVENT_NO_COLLISION, c);
				pPair->mColliding = false;
			}
		}
		else
		{
			if (!penetrationset || intersection < penetration)
			{
				penetration = intersection;
				normal = axis;
			}
		}
	}

	//if this stage is reached, then we have intersections on all axes, and have recorded axis of least penetration
	//and penetration depth

	c.mNormal = normal;
	c.mPenetrationDepth = penetration;
	//calculate contact points
	c.mContactPointA = pointsA[computeFarthestPoint(pointsA, 8, c.mNormal)];
	c.mContactPointA = pointsB[computeFarthestPoint(pointsB, 8, -c.mNormal)];

	//Now handle pass on contact manifold for response
	if (pPair->mColliding)
	{
		e.setCollisionData(EVENT_COLLISION, c);
		pPair->mColliding = true;
	}
	else
	{
		e.setCollisionData(EVENT_COLLISION_BEGIN, c);
		pPair->mColliding = true;
	}

}

void SATMultiCore::setPairManager( std::shared_ptr<PairManager> pManager )
{
	mPairManager = pManager;
}

void SATMultiCore::shutDown()
{
	
}

SATMultiCore::~SATMultiCore()
{
}
