#include "CollisionSolver.h"
#include "NarrowPhase.h"
#include "BroadPhase.h"
#include <algorithm>
#include "SAPSingleCore.h"
#include "SATSingleCore.h"
#include "Application.h"
#include "Profiler.h"
CollisionSolver::CollisionSolver()
{
	mPairmanager = nullptr;
}

void CollisionSolver::addEntity( Entity * pEntity )
{
	mEntities.push_back( pEntity );
	mBroadPhase[ mMode ]->addEntity( pEntity );
}

void CollisionSolver::init( CollisionMode pMode )
{
	//TODO
	//create a narrow phase & broad phase of each type (unique pointer)
	//create concrete implementations of each
	if ( pMode == CPU_SINGLE_THREAD )
	{
		mModeName = "Single_Thread";
	}
	else
	{
		mModeName = "Multi_Thread";
	}
	mPairmanager = std::make_shared<PairManager>();
	mBroadPhase.resize( NUM_MODES );
	mNarrowPhase.resize( NUM_MODES );
	if (pMode == CPU_SINGLE_THREAD)
	{
		mBroadPhase[pMode] = std::make_unique<SAPSingleCore>();
		mNarrowPhase[pMode] = std::make_unique<SATSingleCore>();
	}
	else if(pMode == CPU_MULTI_THREAD)
	{
		mBroadPhase[pMode] = std::make_unique<SAPMultiCore>();
		mNarrowPhase[pMode] = std::make_unique<SATMultiCore>();
	}
	else
	{
		//mBroadPhase[pMode] = std::make_unique<SAP_GPU>();
		//mNarrowPhase[pMode] = std::make_unique<SAT_GPU>();
	}
	for ( int i = 0; i < NUM_MODES; i++ )
	{
		if ( mBroadPhase[ i ] != nullptr )
			mBroadPhase[ i ]->setPairManager( mPairmanager );
		if ( mNarrowPhase[ i ] != nullptr )
			mNarrowPhase[ i ]->setPairManager( mPairmanager );
	}
	mMode = pMode;
}

void CollisionSolver::update( Application & pApp )
{
	PROFILER->startProfileSection( mModeName + "_BroadPhase" );
	mBroadPhase[ mMode ]->update( pApp );
	PROFILER->endProfileSection( mModeName + "_BroadPhase" );

	PROFILER->startProfileSection( mModeName + "_NarrowPhase" );
	mNarrowPhase[ mMode ]->update( pApp );
	PROFILER->endProfileSection( mModeName + "_NarrowPhase" );
}

void CollisionSolver::setSolverMode( CollisionMode pMode )
{
	mMode = pMode;
}

void CollisionSolver::removeEntity( Entity* pEntity )
{
	auto e = std::find( mEntities.begin() , mEntities.end() , pEntity );
	if ( e != mEntities.end() )
	{
		e = mEntities.erase( e );
		for ( int i = 0; i < NUM_MODES;i++ )
		{
			if ( mBroadPhase[ i ] != nullptr )
				mBroadPhase[ i ]->removeEntity( pEntity );
		}
	}
}

void CollisionSolver::setBatchMode()
{
	mBroadPhase[mMode]->setBatchInsertMode();
}

void CollisionSolver::EndBatchMode()
{
	mBroadPhase[mMode]->endBatchInsertMode();
}

void CollisionSolver::shutDown()
{
	mBroadPhase[ mMode ]->cleanUp();
	mNarrowPhase[ mMode ]->shutDown();
	mBroadPhase.clear();
	mNarrowPhase.clear();
	mPairmanager->clear();
}


CollisionSolver::~CollisionSolver()
{

}
